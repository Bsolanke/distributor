
/***** Distributor js *******/

DataTable();
function DataTable() {

    $title = "Distributor List";
    // Datatable defination
    $DataTable = $("#distributor_table").DataTable({
        ajax: "AllDistributor", //Ajax call, it will only work if ajax will return the
        // data in the format of json ex;- {"data":[{"key1":"val1","key2":"val2,....}]},
        info: false,
        autoWidth: false,
        paging: true, // paging is set to false
        columns: [
            //filling the tbody with json data
            {
                data: null
            },
            {
                data: "name"
            },
            {
                data: "email"
            },
            {
                data: "mobile"
            },
            {
                data: "address"
            },
            {
                data: null,
                render: function (data) {

                    //Edit and Delete button added if data is found
                    return  '<form action="accounts/adminlogin" method="post" >' +
                            '<input type="hidden" name="id" value="'+data.id+'">' +
                            '<input type="hidden" name="_token" class="csrfTokan">' +
                            '<input type="hidden" name="email" value="'+data.email+'">' +
                        '<button type="submit" class=" btn btn-success"><i class="fa fa-sign-in fa-fw"></i> login</button></form>';
                }
            },
            {
                data: null,
                render: function (data) {

                    //Edit and Delete button added if data is found
                    return '<a href="vendor/'+data.id+'"><button  id="edit_btn" class="btn btn-primary"><i class="fa fa-edit" ></i> Edit</button></a> ' +
                        '<button id="delete_btn" class="delete_btn btn btn-danger"><i class="fa fa-trash"></i> Delete</button>';
                }
            }
        ],
        "columnDefs": [{
            // disabling ordering and searching false for action column
            //"targets": ,
            "searchable": false,
            "orderable": false
        }],
        sAjaxDataProp: "data",
        "language": { 
            "emptyTable": "No Distributor Available"
        }
    });
    $DataTable.on('order.dt search.dt', function () {
        $DataTable.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

// assigning onclick event for delete button
$("#distributor_table tbody").on("click", '.delete_btn', function () {
    // getting the full data of selected row
    // data variable contains id,f_name,email etc.
    var data = $("#distributor_table").DataTable().row($(this).parents('tr')).data();
    console.log(data);
    // using jqury confirm plugin with yes and no button
    $.confirm({
        title: "Delete Distributor",
        type: "red",
        icon: "fa fa-trash",
        content: "are you sure to Delete this Distributor ?",
        buttons: {
            Yes: {
                btnClass: "btn-red",
                action: function () {
                    //after clicking on yes button it will call a function which delete
                    //will the selected staff using its id
                    delete_table(data['id']);
                },
            },
            No: function () {
                // Nothing will happen after clicking no
            }
        }
    })
});

function delete_table($id) {
    
    var token =$("#token").val();
    // this function will take staff id as parameter and delete the record using that id
    //ajax call
    $type = "red";
    $icon = "fa fa-times";
    $.ajax({
        url: "/DeleteDistributor",
        type: "POST",
        dataType: "json", //the output of ajax will parsed to json
        data: { //passing data to ajax
            "id": $id,
            "_token":token
        },
        success: function ($data) {
            //$data contains the
            if ($data.success === true) {
                //just reloading staff table after deleting a staff
               
                $('#distributor_table').DataTable().ajax.reload(null, false);

                $type = "green";
                $icon = "fa fa-check";
            }
            //Jquery confirm alert used for displaying result or output
            // If delete is success
            $.alert({
                icon: $icon,
                type: $type,
                title: "Delete Distributor",
                content: $data.message
            });
        }

    });
}


setTimeout(function(){  $(".csrfTokan").val($("[name='_token']").val());  }, 1000);

//  Back To Admin Login
// <form id="admin-login-form" action="http://13.126.214.25/tracking/accounts/adminlogin" method="POST" style="display: none;">
//           <input type="hidden" name="id" value="216">
//             <input type="hidden" name="_token" value="e5Senow72pEMV5YbKJ4IcGzgtdZ5WwrOv379SQNP">
//         </form>