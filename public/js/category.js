
/***** category js *******/

$("#addbtncategory").on("click",function(){
    $("#category_modal").modal("show");
    $("#category_form").trigger('reset');
});

//data tabel
data_table();
function data_table() {

    $title = "Category List";
    // Datatable defination
    $data_table = $("#category_table").DataTable({
        ajax: "AllCategory", //Ajax call, it will only work if ajax will return the
        // data in the format of json ex;- {"data":[{"key1":"val1","key2":"val2,....}]},
        info: false,
        autoWidth: false,
        paging: true, // paging is set to false
        columns: [
            //filling the tbody with json data
            {
                data: 'id'
            },
            {
                data: "category_name"
            },
            {
                data: null,
                render: function (data) {
                    return '<img src="'+data.image+'" style="width: 50px; height:50px;" class=""img-thumbnail>';
                }
            },
            {
                data: null,
                render: function (data) {
                    
                    //Edit and Delete button added if data is found
                    return '<button  class="btn btn-primary edit_btn_category"><i class="fa fa-edit"></i> Edit</button>'+' <button id="delete_btn" class="delete_btn btn btn-danger"><i class="fa fa-trash"></i> Delete</button>';
                }
            }
        ],
        "columnDefs": [{
            // disabling ordering and searching false for action column
            //"targets": ,
            "searchable": false,
            "orderable": false
        }],
        sAjaxDataProp: "data",
        "language": {
            "emptyTable": "No Category Available"
        }
    });
    $data_table.on('order.dt search.dt', function () {
        $data_table.column(0, {
            search: 'applied',
            order: 'applied'
        })
        // .nodes().each(function (cell, i) {
        //     cell.innerHTML = i + 1;
        // });
    }).draw();
}


// category from submit 
$("#category_form").on("submit", function (e) {
    //preventing default behavior of form i.e. stops the reload of page after submitting a form
    e.preventDefault();
    $type = "red";
    $icon = "fa fa-times";

    // new ajax call
    $.ajax({
        url: "category/create", //Add building
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        data: new FormData(this),
        success: function ($data) {
            
             $("#category_form").trigger('reset'); // form reset
             if ($data.error == 'false') {
                console.log($data.error);
                $("#category_modal").modal('toggle');
                refresh_table('category_table');
                $type = "green";
                $icon = "fa fa-check";
                
            }
            $.alert({
                // Jquery confirm will display result after ajax
                title: "Add New Category",
                content: $data.message,
                icon: $icon,
                type: $type
            });
            if ($data.error == 'true') {
            $('#errormessage').css('display', 'block');
            $('#errormessagetext').html($data.message);
          }
        }

    });
});
// edit button table click
$("#category_table tbody").on("click", '.edit_btn_category', function () {
    var data = $("#category_table").DataTable().row($(this).parents('tr')).data();
    console.log(data);
    $("#category_modal").modal('show');
    $(".category_form").addClass('');
    $(".edit_category_name").val(data['category_name']);
    $(".edit_category_id").val(data['id']);
});
// update notify 
$("#EditBuildingForm").on("submit", function (e) {

    //preventing default behavior of form i.e. stops the reload of page after submitting a form
    e.preventDefault();
    $type = "red";
    $icon = "fa fa-times";

    // new ajax call
    $.ajax({
        url: "UpdateBuilding", //Update Building
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        data: new FormData(this),
        success: function ($data) {
            console.log($data);
             $("#EditBuildingForm").trigger('reset'); // form reset
             if ($data.status == 'true') {
                $("#EditBuildingModal").modal('toggle');
                refresh_table();
                $type = "green";
                $icon = "fa fa-check";
                
            }
            $.alert({
                // Jquery confirm will display result after ajax
                title: "Edit Building",
                content: $data.message,
                icon: $icon,
                type: $type
            });
            if ($data.status == 'false') {
            $('#message').css('display', 'block');
            $('#message').html($data.message);
            $('#message').addClass($data.class_name);
          }
        }

    });
});




// assigning onclick event for delete button
$("#building_table tbody").on("click", '.delete_btn', function () {
    // getting the full data of selected row
    // data variable contains id,f_name,email etc.
    var data = $("#building_table").DataTable().row($(this).parents('tr')).data();
    console.log(data);
    // using jqury confirm plugin with yes and no button
    $.confirm({
        title: "Delete Building",
        type: "red",
        icon: "fa fa-trash",
        content: "are you sure to Delete this Building ?",
        buttons: {
            Yes: {
                btnClass: "btn-red",
                action: function () {
                    //after clicking on yes button it will call a function which delete
                    //will the selected staff using its id
                    delete_record(data['building_id']);
                },
            },
            No: function () {
                // Nothing will happen after clicking no
            }
        }
    })
});

function delete_record($id) {
    
    var token =$("#token").val();
    // this function will take staff id as parameter and delete the record using that id
    //ajax call
    $type = "red";
    $icon = "fa fa-times";
    $.ajax({
        url: "DeleteBuilding",
        type: "POST",
        dataType: "json", //the output of ajax will parsed to json
        data: { //passing data to ajax
            "id": $id,
            "_token":token
        },
        success: function ($data) {
            //$data contains the
            if ($data.success === true) {
                //just reloading staff table after deleting a staff
                refresh_table();
            
                $type = "green";
                $icon = "fa fa-check";
            }
            //Jquery confirm alert used for displaying result or output
            // If delete is success
            $.alert({
                icon: $icon,
                type: $type,
                title: "Delete Building",
                content: $data.message
            });
        }

    });
}

// refresh table 
function refresh_table($tableName) {
    $("#"+$tableName+"").DataTable().ajax.reload(null, false);
    
}


/*****************************************************************************************************************/
                                          //Sub category  
/*****************************************************************************************************************/

$("#addbtnsubcategory").on("click",function(){
    $("#sub_category_modal").modal("show");
    $("#sub_category_form").trigger('reset');
});
getCategory();
// get Category list
function getCategory() {
    $.ajax({
        url: "AllCategory",
        type: "GET",
        dataType: "json",
        success: function ($data) {
            if ($data.length === 0) {
                return;
            }
            $options = "<option value='' selected disabled>Select Category</option>";
            $.each($data['data'], function ($k, $v) {
                $options += "<option value='" + $v['id'] + "'>" + $v['category_name'] + "</option>"
            });
            $(".categoryselect").html($options);
            $(".categoryselect").select2({
                placeholder: "Select Category"
            });
            
        }
    });
}


data_tablesucound();
function data_tablesucound() {

    $title = "Sub Category List";
    // Datatable defination
    $data_table = $("#sub_category_table").DataTable({
        ajax: "AllSubCategory", //Ajax call, it will only work if ajax will return the
        // data in the format of json ex;- {"data":[{"key1":"val1","key2":"val2,....}]},
        info: false,
        autoWidth: false,
        paging: true, // paging is set to false
        columns: [
            //filling the tbody with json data
            {
                data: null
            },
            {
                data: "sub_category_name"
            },
            {
                data: "category_name"
            },
            {
                data: null,
                render: function (data) {
                    return '<img src="'+data.image+'" style="width: 50px; height:50px;" class=""img-thumbnail>';
                }
            },
            {
                data: null,
                render: function (data) {
                    
                    //Edit and Delete button added if data is found
                    return '<button id="edit_btn" class="btn btn-primary "><i class="fa fa-edit"></i> Edit</button>'+' <button id="delete_btn" class="delete_btn btn btn-danger"><i class="fa fa-trash"></i> Delete</button>';
                }
            }
        ],
        "columnDefs": [{
            // disabling ordering and searching false for action column
            //"targets": ,
            "searchable": false,
            "orderable": false
        }],
        sAjaxDataProp: "data",
        "language": {
            "emptyTable": "No Sub Category Available"
        }
    });
    $data_table.on('order.dt search.dt', function () {
        $data_table.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

// category from submit 
$("#sub_category_form").on("submit", function (e) {
    //preventing default behavior of form i.e. stops the reload of page after submitting a form
    e.preventDefault();
    $type = "red";
    $icon = "fa fa-times";

    // new ajax call
    $.ajax({
        url: "subcategory/create", 
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        data: new FormData(this),
        success: function ($data) {
            
             $("#sub_category_form").trigger('reset'); // form reset
             if ($data.error == 'false') {
                console.log($data.error);
                $("#sub_category_modal").modal('toggle');
                refresh_table('sub_category_table');
                $type = "green";
                $icon = "fa fa-check";
                
            }
            $.alert({
                // Jquery confirm will display result after ajax
                title: "Add New Sub Category",
                content: $data.message,
                icon: $icon,
                type: $type
            });
            if ($data.error == 'true') {
            $('#errormessage').css('display', 'block');
            $('#errormessagetext').html($data.message);

          }
        }

    });
});