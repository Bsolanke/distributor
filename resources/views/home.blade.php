
@extends('layouts.header')


@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    

    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <section>
      @if ( !empty(session('user_admin'))  && ( !isSysAdmin() && isVendor() ))
     
        <form id="admin-login-form left-margin" action="{{ route('account.login') }}" method="POST">
          <input type="hidden" name="id" value="{{ session('user_admin') }}">
            {{ csrf_field() }}
            <button class="btn btn-danger backadminlogin" type="submit"><i class="fa fa-sign-in fa-fw"></i> Back To Admin Login</button>
        </form>
      
    @endif
    </section>

    <!-- Main content -->
    <section class="content">
     <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>0</h3>

              <p>Total Distributor</p>
            </div>
            <div class="icon">
              <i class="ion ion-home"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  @endsection

 