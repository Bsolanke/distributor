
@extends('layouts.header')


@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Distributor
        <small>Edit Distributor panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit Distributor</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     <!-- Small boxes (Stat box) -->
      <div class="row">
         <div class="col-md-10">
            <!-- box start -->
            <div class="box box-info">
              <div class="box-header">
                <a href="{{ url('Distributor') }}" class="btn btn-info">
                <i class="fa fa-users"> </i> All Distributor 
              </a>
              </div>
              <!-- box-body start -->
              <div class="box-body">
                <form class="form" id="adddistributorform" method="POST" action="{{ route('update.distributor') }}">

                  @if(count($errors))
                    <div class="alert alert-warning">
                      <strong>Whoops!</strong> There were some problems with your input.
                      <br/>
                      <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>
                  @endif

                  @include('flash-message')

                  @yield('content') 

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $distributor->id }}">
                      <div class="col-md-6">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                          <label for="name"> Name</label>
                          <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="{{ $distributor->name }}" >
                          <span class="text-danger">{{ $errors->first('name') }}</span>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                          <label for="email"> Email</label>
                          <input type="email" class="form-control" name="email" id="email" placeholder="Enter email" value="{{ $distributor->email }}">
                          <span class="text-danger">{{ $errors->first('email') }}</span>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group ">
                          <label for="mobile"> Mobile</label>
                          <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Enter Mobile" value="{{ $distributor->mobile }}">
                        </div>
                      </div>

                      

                      <div class="col-md-6">
                        <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                          <label for="address"> Address</label>
                           <textarea class="form-control" rows="3" placeholder="Enter Address" id="address" name="address" >{{ htmlspecialchars($distributor->address) }}</textarea> 
                          <span class="text-danger">{{ $errors->first('address') }}</span>
                        </div>
                      </div>

                      <div class="col-md-6 col-md-offset-3" >
                        <div class="col-md-6">
                          <button type="submit" class="btn btn-success form-control" id="btnsubmit" >Update</button>
                        </div>

                        <div class="col-md-6">
                          <button type="reset" class="btn btn-warning form-control" id="btnreset" >Rsest</button>
                        </div>
                     </div>
                     <!-- /col6 -->
                </form>
                <!-- /form -->
              </div>
              <!-- / box-body -->
            </div>
            <!-- box end -->
         </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
  @endsection


            
