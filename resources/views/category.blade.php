
@extends('layouts.header')


@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Category Manage
        <small>Category Manage panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Category Manage</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-12">
              <div class="box box-info">
                  <div class="box-header">
                    @include('flash-message')

                    @yield('content') 
                    

                    
                  </div>
                  <div class="box-body">

                        <div class="nav-tabs-custom">
                          <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab">Category</a></li>
                            <li><a href="#tab_2" data-toggle="tab">Sub Category</a></li>
                            
                            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                          </ul>
                          <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">

                            <div class="row margin-bottom">
                              <div class="col-md-12">
                                  <button type="button" id="addbtncategory"  class="btn btn-primary" >
                                    <i class="fa fa-plus"></i> Add New Category 
                                  </button>
                              </div>
                            </div>

                                <div class="table-responsive">
                                        <table id="category_table" class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Category Name</th>
                                                    <th>image</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Category Name</th>
                                                    <th>image</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">

                                <div class="row margin-bottom">
                                  <div class="col-md-12">
                                      <button type="button" class="btn btn-primary" id="addbtnsubcategory">
                                        <i class="fa fa-plus"></i> Add New Sub Category 
                                      </button>
                                  </div>
                               </div>

                                <div class="table-responsive">
                                        <table id="sub_category_table" class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Sub Category Name</th>
                                                    <th>Category Name</th>
                                                    <th>image</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Sub Category Name</th>
                                                    <th>Category Name</th>
                                                    <th>image</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                            </div>
                            
                          </div>
                          <!-- /.tab-content -->
                        </div>

                      
                  </div>
                  <!-- /.box -->

              </div>
              <!-- /.col (left) -->
          </div>
          <!-- /col12 -->
          <!-- _token -->
          <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

        <!-- modal category -->
        <div class="modal fade" id="category_modal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Category</h4>
              </div>
              <form class="category_form" id="category_form">
                <div class="modal-body">

                  <div class="errormessage" id="errormessage">
                      <p class="text-danger" id="errormessagetext">
                        
                      </p>
                  </div>
                   @csrf   
                  <div class="col-md-12">
                      <div class="form-group ">
                        <label for="category"> Category Name</label>
                        <input type="text" class="form-control" name="category" id="category" placeholder="Enter Category Name" value="">
                        <span class="text-danger"></span>
                      </div>

                      <div class="form-group ">
                        <label for="image"> Image</label>
                        <input type="file" class="form-control" name="image" id="image" placeholder="Cheuse" value="">
                        <span class="text-danger"></span>
                      </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Save changes</button>
                  <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
                </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal category -->


        <!-- modal sub category -->
        <div class="modal fade" id="sub_category_modal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Sub Category</h4>
              </div>
              <form class="sub_category_form" id="sub_category_form">
                <div class="modal-body">
                  @csrf
                  <div class="col-md-12">
                      <div class="form-group ">
                        <label for="categoryselect"> Category</label>
                        <select id="categoryselect" name="category_id" class="form-control categoryselect" required style="width: 100%">
                          <option></option>
                        </select>
                        <span class="text-danger"></span>
                      </div>

                      <div class="form-group ">
                        <label for="category"> Sub Category</label>
                          <input type="text" placeholder="Enter Sub Category" name="sub_category" id="sub_category" class="form-control" required>
                        <span class="text-danger"></span>
                      </div>

                      <div class="form-group ">
                        <label for="image"> Image</label>
                        <input type="file" class="form-control" name="image" id="image" placeholder="Cheuse" value="">
                        <span class="text-danger"></span>
                      </div>

                  </div>

                 


                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Save changes</button>
                  <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
                </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal sub category -->




  
  @endsection