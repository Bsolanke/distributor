<?php

//header file sitebar
return [
		// side bar usd
		'All_Society'=>'All Society',
		'Add_Society'=>'Add Society',
		'Edit_Society'=>'Edit Society',
		// side bar use
		'Noticeboard'=>'Noticeboard',
		'All_Notice'=>'All Notice',
		'Add_Notice'=>'Add Notice',
		'Edit_Notice'=>'Edit Notice',



	    'Society'=>'Society',
		'All Society'=>'Society',
		'Add Society'=>'Add New Society',
		'Add New Society'=>'Add New Society',
		//dashboard
		'new customers' =>'New Customers',
		'new security'=>'New Security',



		//addsociety
		'name'=>'Society Name',
		'Edit_Security'=>'Edit Security',
		'email'=>'Email',
		'society admin'=>'Society Admin',
		'society adminemail'=>'Admin Email',
		'address'=>'Society Address',
		'password'=>'Password',
		'confirmed password'=>'Confirmed Password',
		'gate'=>'How Many Sequery Gate Apartments/Building',
		'all society'=>'All Society',
		//edit society
		'edit society'=>'Edit Society',
		'change password'=>'Change Password',

		/* complaints type */
		'complaints type'=>'Complaints Type', // side bar
		

		//Customer
		 // site-bar used
		'All_Customer'=>'All Customer',
		'Add_Customer'=>'Add Customer',
		'Edit_Customer' =>'Edit Customer',

		'customer'=>'Customer',
		'add new members'=>'Add New Members',
		'all customer'=>'All Customers',
		'customer name'=>'Customer Name',
		'mobile'=>'Mobile No',
		'flat'=>'Flat Size',
		'owners'=>'Owners',
		'members'=>'Members',
		'flat no'=>'Flat No',
		'building'=>'Building No/Name',
		'add new customer'=>'Add New Customer',
		'edit customer'=>'Edit Customer',

		// security 
		//side-bar ue
		'Security'=>'Security',
		'All_Security'=>'All Security',
		'Add_Security'=>'Add Security',

		'security'=>'Security',
		'add new security'=>'Add New Security',
		'all security'=>'All Security',
		'security name'=>'Security Name',
		'security gate'=>'Security Gate',
		'security name'=>'Security Name',
		'security office'=>'Security Office',

		/* noticeboard */
		'noticeboard'=>'Noticeboard',
		'add new notice'=>'Add New Notice',
		'add new notice'=>'Add New Notice',
		'all notice'=>'All Notice',
		'notice title'=>'Title',
		'notice'=>'Notice',
		'expriy date'=>'Expriy Date',
		'expriy time'=>'Time',

		// visitor sideabar
		'visitor'=>'Visitor',
		'visitor_type'=>'Visitor Type',
		'visitor_company'=>'Visitor Company',

		// visitor company 
		'visitor company'=>'Visitor Company',
		'add visitor company'=>'Add Visitor Company',
		'edit visitor company'=>'Edit Visitor Company',
		'add new company'=>'Add New Company',

		// add company form
		'company name'=>'Company Name',
		'company type'=>'Type',
		'company logo'=>'Company Logo',


		/* Visitor  type */
		'visitor type'=>'Visitor Type',
		'add visitor type'=>'Add Visitor Type',

		// visitor type form
		'add new visitor type'=>'Add New Visitor Type',
		'edit visitor type'=>'Edit Visitor Type',
		'visitor type'=>'Visitor Type',
		'visitor type logo'=>'Logo'





       ];
