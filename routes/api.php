<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//******  Superadmin Api  ******* //
Route::prefix('superadmin/v1')->group(function () {


});

//******  Admin Api  ******* //
Route::prefix('admin/v1')->group(function () {

Route::post('/login','Api\Admin\ApiAdminController@loginUser'); // login or logout api
Route::post('/verifyOtp','Api\Admin\ApiAdminController@verifyOtp');  // verifyOtp api


});

// ******  User Api ****** //
Route::prefix('user/v1')->group(function () {


/*** login logout ***/

Route::post('/login','Api\User\ApiUserController@loginUser'); // login or logout api
Route::post('/verifyOtp','Api\User\ApiUserController@verifyOtp'); // verifyOtp api
Route::post('/fcm_register','Api\User\ApiUserController@fcm_register'); // fcm Register / update 



/****  Visitor  ****/

Route::get('/visitorCompanyList','Api\User\ApiVisitorsController@visitorCompanyList');  //Visitor company list
Route::post('/addVisitor','Api\User\ApiVisitorsController@CreateVisitor');  // add/Create Visitor
Route::get('/visitorslist','Api\User\ApiVisitorsController@visitorslist');  //visitor list 
Route::post('/visitorUpdate','Api\User\ApiVisitorsController@visitorUpdate'); // Update Visitor
Route::post('/visitorDelete','Api\User\ApiVisitorsController@visitorDelete'); // Delete Visitor


/*** Noticeboard ****/
Route::get('/noticeboard','Api\User\ApiUserController@noticeboard'); // all notice

/*******  Members   ********/
Route::get('/members','Api\User\ApiUserController@allMembers'); // user in society Members list


/*******  Complaint   ********/
Route::get('/complaintsType','Api\User\ApiComplaintsController@complaintsType'); // complaints Type
Route::get('/ComplaintsList','Api\User\ApiComplaintsController@ComplaintsList'); // complaint List
Route::post('/addComplaint','Api\User\ApiComplaintsController@createComplaint'); // create / add complaints



/********** Settings  *************/
Route::get('/settings','Api\User\ApiSettingsController@GetSettings'); // all settings
Route::post('/changePassword','Api\User\ApiSettingsController@changePassword');  // password update
Route::post('/notificationAllow','Api\User\ApiSettingsController@notificationAllow'); // notifications on off

/*******  Vehicles   ********/
Route::get('/vehiclesList','Api\User\ApiVehiclesController@vehiclesList'); // vehicle list
Route::post('/addVehicle','Api\User\ApiVehiclesController@createVehicle');  // add/create vehicle
Route::post('/deleteVehicle','Api\User\ApiVehiclesController@deleteVehicle'); // delete Vehicle

/************ My Advantisment **************/
Route::get('/myAddsList','Api\User\ApiMyAddController@myAddsList'); // Advantisment list


/*************** Trending Stores ************/
//add / created Trending Stores
Route::post('/addTrendingStores','Api\User\ApiTrendingStoresController@createdTrendingStores');
// Trending Stores List
Route::get('/trendingStoresList','Api\User\ApiTrendingStoresController@trendingStoresList');
// trending Stores Delete trendingStoresDelete
Route::get('/trendingStoresDelete','Api\User\ApiTrendingStoresController@trendingStoresDelete');

/*************** Api Dashboard  ************/
Route::get('/getDashboard','Api\User\ApiDashboardController@getDashboard');

});



// ***** Security *****//
Route::prefix('security/v1')->group(function () {
// login api
Route::post('/login','Api\Security\ApiSecurityController@loginSecurity');

// verifyOtp api
Route::post('/verifyOtp','Api\Security\ApiSecurityController@verifyOtp');
//fcm Register // update
Route::post('/fcm_register','Api\Security\ApiSecurityController@fcm_register');
// visiter List
Route::get('/visitorList','Api\Security\ApiSecurityController@visitorList');
// visitor verifyotp
Route::post('/verifyVisitorOtp','Api\Security\ApiSecurityController@verifyVisitorOtp');

});

