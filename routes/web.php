<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/***** SuperAdmin ****/ 
// multi login
Route::post('/accounts/adminlogin','HomeController@adminUserLogin')->name('account.login');

/******** Distributor **********/
//show
Route::get('/Distributor','DistributorController@index')->name('Distributor');
// get all
Route::get('/AllDistributor','DistributorController@GetAllDistributor')->name('AllDistributor');
// return add Distributor view 
Route::get('/Add-Distributor', function () { return view('adddistributor'); })->middleware('auth');
// create Distributor
Route::post('/CreateDistributor','DistributorController@Create')->name('CreateDistributor');
// edit view Distributor
Route::get('/Distributor/{id}', 'DistributorController@editVendor')->name('edit.distributor'); //edit form
// update Distributor
Route::post('/UpdateDistributor','DistributorController@Update')->name('update.distributor');
// Delete Distributor
Route::post('/DeleteDistributor','DistributorController@DeleteDistributor')->name('DeleteDistributor');

/******* Distributor End *********/


