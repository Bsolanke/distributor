<?php 
use Illuminate\Support\Facades\DB;

// push notification
if (! function_exists('PushNotification')) 
{
    function PushNotification($token,$message,$mobile,$app_type)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        //$token='cPHr8ktYVtE:APA91bE0EPdh-5SMxwdbqORysShFie_rfXUDs0DO4m3_z8dB_62_vej3SCYC7Z4aM4cSgwh4XDUh-Cra57KKAja3mIL490tn4JjOH8JM9bzJM8tk81wTAdF2Skg_5ehW9TyJ9XPU2cof';    
        $notification = [
            'body' => $message,
            'sound' => true,
        ];
        
        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];
        if ($app_type === 'user') {
          // user app key
          $key = 'AAAAFZXY6Yw:APA91bHuB91FamPXGU99eQkQlS-sz7cOJkvM6NHJLPTliMwNM0lBODAP5ZkX_CsehO2dhu9-hbdDDK0AsXqxKfoZ_LQT_6NKg7qbjuZz50gtctY2P4izR0Qihrdt6m0BVEa6fqNzIIAH';
          
        }elseif ($app_type === 'admin') {
          //admin app key 

        }elseif ($app_type === 'security') {
          //security app key
          $key = 'AAAAoc7ViU8:APA91bE40UI1birMqgQliitgsAZ8VT1CQfdUHGSKM4tA4CDkAyVr5Dpyf4rEJr2cRWHZJwthNZ3G3sANuksj-mIXPHiP01vMS7ViBIa-lGjNxPmLtQe827FJXvcoK1Ou9-adzGjpjdTR';
        }
        
        
        $headers = [
            'Authorization: key='.$key,
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        
        $json = json_decode(curl_exec($ch), true); 

          $success = $json['success'];
          $results = $json['results'];
          $failure = $json['failure'];
        //log if message id / error InvalidRegistration
         if (isset($results[0]['message_id'])) {
            $log = $results[0]['message_id'];

         }elseif (isset($results[0]['error'])) {
             $log = $results[0]['error'];
         }
       // insert log table  
    	$dataarr = array('notification_message' =>$message,'logs'=>$log,'is_success'=>$success,'failure'=>$failure,'mobile'=>$mobile, 'created_at'=>date('Y-m-d H:i:s'));
    	$sql = DB::table('logs')->insert($dataarr);
        curl_close($ch);
        return $result = $success;
      }
}
// get user timezon
if (! function_exists('UserTimezone')) 
{
    function UserTimezone($user_id){
        $user = DB::table('customers')
               ->where('cust_id',$user_id)
               ->first();
       $timezon = $user->time_zone;

        if (isset($timezon) && $timezon !="") {
           return $user->time_zone;
        }else{
           return "Asia/Kolkata";
        }
    }
}


