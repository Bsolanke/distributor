<?php

namespace Distributor\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Auth;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     /*
       load view Category index
     */
    public function index(){

     	return view('category');
    }
    /*
     get all Category
    */
    public function allCategory(){
     
      $datasql=DB::table('category')
      //->where('manager_id',)
      ->get();

      $data['data'] = $datasql;
      // set data 
      if ($datasql) {
         echo json_encode($data);
      }else{
         echo "{\"data\":[]}";
      }
    }


  // category Create 
  public function categoryCreate(Request $request){

      $category = $request['category'];

      $validation = Validator::make($request->all(), [
      'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
      'category' => 'required'
      ]);

       if($validation->passes())
       {
          $image = $request->file('image');

          $new_name = rand() . '.' . $image->getClientOriginalExtension();
          $image->move(public_path('upload/images/category/'), $new_name);
          $path = 'upload/images/category/'.$new_name;

          $data = array('category_name'=>$category,
                        'image' => $path,
                        'manager_id'=>Auth::user()->id,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s') 
                      );

          $insertId = DB::table('category')->insertGetId($data);
          
          if ($insertId) {
              $data = array('error' =>'false','message'=>'Category Added Successfully.');
              return response()->json($data); 
          }else{
                $data = array('error' =>'true','message'=>'Category Not Added!');
                return response()->json($data);
          }
       }
       else
       {
          $Mesaage = array('error'=>'true','message' => $validation->errors()->all(),'class_name'  => 'alert-danger');
          return response()->json($Mesaage);
       }            
    }
  /*
     delete Category
   */ 
   public function DeleteCategory(Request $request){

    $sqlDelete = DB::table('category')
    ->where('id',$request['id'])
    ->delete();

    if ($sqlDelete >0) {
      $data = array('success' => true,'message'=>'Complaint Deleted Successfully.');
      return response()->json($data); 
    }else{
      $data = array('success' => false,'message'=>'Complaint Not Deleted!');
      return response()->json($data);
    }

   }

   /****************************************************************************/
                                   //sub category
   /****************************************************************************/

    /*
     get all sub Category
    */
  public function allSubCategory(){
     
      $datasql=DB::table('sub_category as sc')
      ->leftJoin('category as c','sc.category_id','c.id')
      ->select('sc.*','c.category_name')
      ->get();

      $data['data'] = $datasql;
      // set data 
      if ($datasql) {
         echo json_encode($data);
      }else{
         echo "{\"data\":[]}";
      }
  }

  // sub category Create 
  public function subcategoryCreate(Request $request){

      $category_id = $request['category_id'];
      $sub_category = $request['sub_category'];

      $validation = Validator::make($request->all(), [
      'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
      'category_id' => 'required',
      'sub_category'=>'required'
      ]);

       if($validation->passes())
       {
          $image = $request->file('image');

          $new_name = rand() . '.' . $image->getClientOriginalExtension();
          $image->move(public_path('upload/images/category/'), $new_name);
          $path = 'upload/images/category/'.$new_name;

          $data = array('sub_category_name'=>$sub_category,
                        'category_id'=>$category_id,
                        'image' => $path,
                        'manager_id'=>Auth::user()->id,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s') 
                      );

          $insertId = DB::table('sub_category')->insertGetId($data);
          
          if ($insertId) {
              $data = array('error' =>'false','message'=>'Sub Category Added Successfully.');
              return response()->json($data); 
          }else{
                $data = array('error' =>'true','message'=>'Sub Category Not Added!');
                return response()->json($data);
          }
       }
       else
       {
          $Mesaage = array('error'=>'true','message' => $validation->errors()->all(),'class_name'  => 'alert-danger');
          return response()->json($Mesaage);
       }            
    }


     

}
