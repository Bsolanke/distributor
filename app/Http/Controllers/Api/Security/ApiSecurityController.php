<?php

namespace Distributor\Http\Controllers\Api\Security;

use Illuminate\Http\Request;
use Distributor\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;

class ApiSecurityController extends Controller
{
    /*
      login user api
    */
    public function loginSecurity(Request $request)
    {
      header('Content-Type: application/json');

      //logout params
      if (isset($request['isLogout'])) {
        $isLogout = $request['isLogout'];
      }

      if( isset($request['mobile']) && $request['mobile']!="")
      {
             // check mobile no exist
             $MobileExist=DB::table('security')
             ->where('mobile','=',$request['mobile'])
             ->where('deleted_flag',0)
             ->get();
  			
             if (count($MobileExist)>0) 
             {
               //$otp = mt_rand(1000,9999); rand gen
               $otp = 1234;  //static otp
               // save otp 
               $dataOtp = array('otp' => $otp, 'mobile' =>$request['mobile']);
               //$save=ApiUserModel::saveOtp($dataOtp);
               DB::table('saveotp')->insert($dataOtp);
               $lastId = DB::getPdo()->lastInsertId();
               if(isset($lastId))
               {
         	         $data = array(
            			  "success" =>true,
            			  "otp"=>$otp,
            			  "message" => "Send Otp Successfully",
            			);
          			return response()->json($data);
               }
               else
               {
                  $data = array(
              		"success" =>false,
              		"message" => "Data Not Found",
             			 );
        			    return response()->json($data);
        		   }
             }else
             {
                $data = array(
                "success" =>false,
                "message" => "Mobile no is not exist",
                 );
                return response()->json($data);
             }
          
      }elseif ( $isLogout == 1 ) 
      {
            //user logged out
            $rows = DB::table('fcm_customer')->where('fcmKey', $fcmKey)->where('mobile', $mobile)->delete();    
      }else
      {
          $data = array(
      		"success" =>false,
      		"message" => "All Data Required",
     			 );
    			return response()->json($data);
       }	
     }

     /*
       otp verifyOtp Securtiy
     */
     public function verifyOtp(Request $request){
      header('Content-Type: application/json');

      if( isset($request['mobile']) && $request['mobile']!="" && isset($request['otp']) && $request['otp']!="")
      {
        //check otp 
        $check_otp=DB::table('saveotp')
        ->where('mobile','=',$request['mobile'])
        ->where('otp','=',$request['otp'])
        ->get();
  
        if (count($check_otp)>0) {
          // verify after delete entry
          DB::table('saveotp')->where('mobile','=',$request['mobile'])
          ->where('otp','=',$request['otp'])->delete();
          //user data 
          $UserData=DB::table('security')
          ->where('mobile','=',$request['mobile'])
          ->get();

          $data = array(
            "success" =>true,
            "data"=>$UserData,
            "message" => "login Successfully",
             );
          return response()->json($data);
        }else
        {
          $data = array(
            "success" =>false,
            "data"=>null,
            "message" => "Invalid Otp",
             );
          return response()->json($data);
        }
      }else
      {
        $data = array(
        "success" =>false,
        "message" => "All Data Required",
         );
        return response()->json($data);
      }

     }

    /* 
     FCM Register/Update of Security
    */
    public function fcm_register(Request $request) {
    
        header('Content-Type: application/json');

        $email = $request['email'];
        $account_id = $request['account_id'];
        $password_token = $request['password_token'];

        $unique_phone_id = $request['unique_phone_id'];
        $fcm_server_id = $request['fcm_server_id'];
        $device_details = $request['device_details'];

        $phone_type = $request['phone_type'];
        $fcmKey = $request['fcmKey'];

        $mobile = $request['mobile'];
        
        if ( empty($mobile) || empty($account_id) || empty($password_token) ||  empty($fcmKey) || empty($phone_type) ) {
            
            return json_encode( array('error' => true, 'message'=>'Invalid Parameters', 'mobile' => $mobile,'id' => $account_id, 'password_token' => $password_token) );
        }
       //Verify user
       $user = $this->verifyUser($account_id, $mobile);
        // if no user, send error    
        if (  empty($user) && count($user) < 1 )   {
          //for API call, 
          //return invalid credentails error for user not found in db or invalid password or email all cases
          return json_encode(array('success' => false, 'message'=>'Invalid Credentials', 'email' => $email ));
        }
        //Always try to update first on the basis of device phone id if its failed then insert new registration id
        if ( !empty($unique_phone_id)  ) {

          $message = "Welcome To The LiveGate App";
          $app_type= 'security';
          // push PushNotification call 
          $notification_status = PushNotification($fcmKey, $message,$mobile,$app_type);
          // set notification status 
          if ($notification_status === 1) {
            $notification_status = true;
          }elseif ($notification_status === 0) {
           $notification_status = false;
          }
          ///var_dump($user['']);die();
          $affectedRows = DB::table('fcm_customer')
            //->where('unique_phone_id', $unique_phone_id)
            ->where('mobile', $mobile)
            ->update(['fcmKey' => $fcmKey, 'device_details' => $device_details, 'updated_at' => date('Y-m-d H:i:s')]);
            
            if ( $affectedRows > 0 ) {

                return json_encode(array('success' => true, 'message' => 'Notification key updated!', 'fcm_server_id' => 0, 'notification_status'=>$notification_status ));

            }else{

                $id = DB::table('fcm_customer')->insertGetId(
                    ['email' => $email, 'mobile'=> $mobile, 'account_id' => $account_id, 'unique_phone_id' => $unique_phone_id, 'fcmKey' =>$fcmKey, 'phone_type' => $phone_type, 'device_details' => $device_details,'pushnotificationios'=>$message, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s') ]
                );

                return json_encode(array('success' => true, 'message' => 'Notification key created.', 'fcm_server_id' => $id,'notification_status'=>$notification_status ));    
            }

        }else{

            return json_encode(array('success' => false, 'message' => 'Empty phone id', 'fcm_server_id' => 0 ));    
        }
   }
   /*
     Verify API user
    */
    function verifyUser($account_id, $mobile)
    {
      //verify user only
        //Cache query for 24 hours
        $user = DB::table('security')->where('secu_id',$account_id)->where('mobile', $mobile)->first();
        return $user;
    }
    /*
     Visiter list
    */ 
    public function visitorList(Request $request) {
  
        header('Content-Type: application/json');
        $manager_id = $request['manager_id'];
        $security_id = $request['security_id'];

        if (isset($manager_id) && $manager_id !="" && isset($security_id) && $security_id !="") {
                  
            $visitors=DB::table('visitors as v')
            ->leftJoin('customers as c', 'v.cust_id', '=', 'c.cust_id')
            ->select('v.*','c.customer_name','c.flat_no','c.profile_img','c.building')
            ->where('v.manager_id','=',$manager_id)
            ->where('v.deleted_flag','=',0)
            ->get();
           
        	  //emty array
        	  $data = array();
	          foreach ($visitors as $value) {

                //get visitor type name
                $visitor_type = DB::table('visitor_company')->where('id',$value->comp_id)->where('deleted_flag',0)->first();
                $visitor_type_name = null;
                if (isset($visitor->type)) {
                   $visitor_type_name = $visitor->type;
                }else{
                   $visitor_type_name = null;
                }

                //get company name
                $visitor_company = DB::table('visitor_type')->where('id',$value->type_id)->where('deleted_flag',0)->first();
                $visitor_company_name = null;
                $visitor_company_logo = null;
                if (isset($visitor->type)) {
                   $visitor_company_name = $visitor_company->company_name;
                   $visitor_company_logo = $visitor_company->company_logo;
                }else{
                   $visitor_company_name = null;
                   $visitor_company_logo = null;
                }

                $data[] = array('visitor_id'=>$value->vist_id,
                                'visitor_type_id'=>$value->type_id,
                                'visitor_type_name' => $visitor_type_name,
                                'visitor_name'=>$value->visitor_name,
                                'company_id'=>$value->comp_id,
                                'company_name'=>$visitor_company_name,
                                'visitor_company_logo'=>$visitor_company_logo,
                                'vehicle_no'=>$value->vehicle_no,
                                'visitor_mobile'=>$value->visitor_mobile,
                                'gate'=>$value->gate,
                                'status'=>$value->status,
                                'customer_name'=>$value->customer_name,
                                'building'=>$value->building,
                                'flat_no'=>$value->flat_no,
                                'profile_img'=>$value->profile_img,
                                'otp'=>$value->otp,
                                'date_time'=>$value->date_time,
                                'expriy_date_time'=>$value->expriy_date_time,
                                'created_at'=>$value->created_at
                              );

	          }
            
            if(count($visitors) >0)
            {
                      $data = array("success" =>true,'data'=>$data,"message" => "Record Fetch Successfully",);
                      return response()->json($data);
            }else{
                     $data = array("success" =>false,'data'=>null,"message" => "Data Not Found!",);
                      return response()->json($data);
        }
            }

    }

    /*
      Securtiy confirm Otp
    */
      public function verifyVisitorOtp(Request $request){

        $otp = $request['otp'];
        $manager_id = $request['manager_id'];
        if (isset($otp) && $otp !="" && isset($manager_id) && $manager_id !="") {
          
             $value=DB::table('visitors as v')
            ->leftJoin('customers as c', 'v.cust_id', '=', 'c.cust_id')
            ->select('v.*','c.customer_name','c.flat_no','c.profile_img','c.building')
            ->where('v.manager_id','=',$manager_id)
            ->where('v.otp',$otp)
            ->where('v.deleted_flag','=',0)
            ->first();
          
            //dd($value);die();
            
            $status = null;
            if (count($value) >0) {
               $status = $value->status;

               $visitor_type = DB::table('visitor_company')->where('id',$value->comp_id)->where('deleted_flag',0)->first();
                $visitor_type_name = null;
                if (isset($visitor->type)) {
                   $visitor_type_name = $visitor->type;
                }else{
                   $visitor_type_name = null;
                }

                //get company name
                $visitor_company = DB::table('visitor_type')->where('id',$value->type_id)->where('deleted_flag',0)->first();
                $visitor_company_name = null;
                $visitor_company_logo = null;
                if (isset($visitor->type)) {
                   $visitor_company_name = $visitor_company->company_name;
                   $visitor_company_logo = $visitor_company->company_logo;
                }else{
                   $visitor_company_name = null;
                   $visitor_company_logo = null;
                }

                $data[] = array('visitor_id'=>$value->vist_id,
                                'visitor_type_id'=>$value->type_id,
                                'visitor_type_name' => $visitor_type_name,
                                'visitor_name'=>$value->visitor_name,
                                'company_id'=>$value->comp_id,
                                'company_name'=>$visitor_company_name,
                                'visitor_company_logo'=>$visitor_company_logo,
                                'vehicle_no'=>$value->vehicle_no,
                                'visitor_mobile'=>$value->visitor_mobile,
                                'gate'=>$value->gate,
                                'status'=>$value->status,
                                'customer_name'=>$value->customer_name,
                                'building'=>$value->building,
                                'flat_no'=>$value->flat_no,
                                'profile_img'=>$value->profile_img,
                                'otp'=>$value->otp,
                                'date_time'=>$value->date_time,
                                'expriy_date_time'=>$value->expriy_date_time,
                                'created_at'=>$value->created_at
                              );

               $data = array("success" =>true,'data'=>$data,'status'=>$status,"message" => "Approvat Successfully",);
               return response()->json($data);
              # code...
            }else{
               $data = array("success" =>false,'data'=>null,'status'=>$status,"message" => "Invalid Otp!",);
                return response()->json($data);
            }

        }else{

          $data = array("success" =>false,"message" => "Invalid Parameters!",);
          return response()->json($data);

        }
      }


}
