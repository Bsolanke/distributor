<?php

namespace Distributor\Http\Controllers\Api\user;

use Illuminate\Http\Request;
use Distributor\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Hash;

class ApiVisitorsController extends Controller
{

   /*************   Visitors   **************/
    /*
      visitor Company List
    */
    public function visitorCompanyList(Request $request){
      header('Content-Type: application/json');

      //$visitor_type =  $request['visitor_type'];
      // if (isset($visitor_type) && $visitor_type !="") 
      // {
       
           $company = DB::table('visitor_company')
           ->where('deleted_flag',0)
           ->get();

           $data = array();// 

           foreach ($company as $value) {

             $row = DB::table('visitor_type')->where('id',$value->type)->where('deleted_flag',0)->first();

             $data[] = array('company_id' =>$value->id ,'type'=>$row->type,'company_name'=>$value->company_name ,'logo'=>$value->company_logo);
           }
           if (count($company) >0 ) {

                  $data = array("success" =>true,'data'=>$data,"message" => "Record Fetch Successfully",);
                  return response()->json($data);

               }else{
                     $data = array("success" =>false,'data'=>null,"message" => "Data Not Found!",);
                     return response()->json($data);
               } 
      // }else{
      //       $data = array("success" =>false,'data'=>null,"message" => "Invalid Parameters!",);
      //       return response()->json($data);
      // }
    }
    /*
    Create New Visitor
    */ 
    public function CreateVisitor(Request $request) {
  
      header('Content-Type: application/json');

      $visitor_name = $request['visitor_name'];
      $visitor_type = $request['visitor_type'];
      $visitor_company = $request['company_name'];
      $visitor_mobile = $request['visitor_mobile'];
      $vehicle_no = $request['vehicle_no'];
      $visitor_date_time = $request['visitor_date_time'];
      $gate = $request['gate'];
      $manager_id = $request['manager_id'];
      $user_id = $request['user_id'];

      if ($visitor_type !="" && $user_id !="") {

        $otp = mt_rand(1000,9999);
    
        // current date
        $timezone = UserTimezone($user_id);

        $expriy_date_time = date('Y-m-d H:i:s', strtotime($visitor_date_time));
      
        $data = array('visitor_name' => $visitor_name,'type_id' => $visitor_type,'comp_id' => $visitor_company,'visitor_mobile'=>$visitor_mobile,'vehicle_no' => $vehicle_no,'expriy_date_time'=>$expriy_date_time,'date_time'=>$visitor_date_time,'gate' =>$gate,'otp'=>$otp,'manager_id'=>$manager_id,'cust_id'=>$user_id,'created_at'=> date('Y-m-d H:i:s'),'updated_at'=> date('Y-m-d H:i:s'));

        $sqlId = DB::table('visitors')->insertGetId($data);
        if($sqlId!=0){
                  //get mangerid query
                  $user_id = $request['user_id'];
                  $sqlmanger = DB::table('customers')
                  ->where('cust_id',$user_id)
                  ->where('deleted_flag',0)
                  ->first();

                  //get manager id
                  $manager_id = $sqlmanger->manager_id;
                  // get sequerity query
                  $securitys = DB::table('security')
                  ->where('manager_id',$manager_id)
                  ->where('deleted_flag',0)
                  ->get();

                  $id = array();  
                  $mobile = array();
                  //for each used mulit array get id 
                  foreach ($securitys as $security) {
                    $id[]=$security->secu_id;
                    $mobile[]=$security->mobile;
                  }
                  // get fcm key/tokan query
                  $fcms = DB::table('fcm_customer')
                  ->whereIn('account_id',$id)
                  ->whereIn('mobile',$id)
                  ->get();
                  //emty array
                  $data = array();
                  $notification_status = false;
                  foreach ($fcms as $fcm) {
                    $fcmKey = $fcm->fcmKey;
                    $mobile = $fcm->mobile;
                    //var_dump($mobile);
                    
                    $message = "Test message";
                    // push PushNotification call 
                    $app_type ='security';
                    $notification_status = PushNotification($fcmKey, $message,$mobile,$app_type);
                    
                    // set notification status 
                    if ($notification_status === 1) {
                      $notification_status = true;
                    }elseif ($notification_status === 0) {
                     $notification_status = false;
                    }

                  }
                  
                  $data = array("success" =>true,"message" => "Visitor Add Successfully",'notification_status'=>$notification_status);
                  return response()->json($data);
            }else
            {
                 $data = array("success" =>false,"message" => "Data Not Found");
                  return response()->json($data);
            }
        }else{
                  $data = array("success" =>false,"message" => "Invalid Parameters");
                  return response()->json($data);
        }    
    }
    /*
     visitor Update
    */
     public function visitorUpdate(Request $request)
     {
      header('Content-Type: application/json');

      $visitor_name = $request['visitor_name'];
      $visitor_type = $request['visitor_type'];
      $visitor_company = $request['company_name'];
      $visitor_mobile = $request['visitor_mobile'];
      $vehicle_no = $request['vehicle_no'];
      $visitor_date_time = $request['visitor_date_time'];
      $gate = $request['gate'];
      $manager_id = $request['manager_id'];
      $user_id = $request['user_id'];

      $timestamp = Carbon::now()->timestamp;
      $data = array();
      $visiter_id= $request['visitor_id'];
      
      if( isset($request['visitor_id']) && $request['visiter_name']!="" && isset($request['visiter_type']) )
      {

        $expriy_date_time = date('Y-m-d H:i:s', strtotime($visitor_date_time));
      
        $data = array('visitor_name' => $visitor_name,'type_id' => $visitor_type,'comp_id' => $visitor_company,'visitor_mobile'=>$visitor_mobile,'vehicle_no' => $vehicle_no,'expriy_date_time'=>$expriy_date_time,'date_time'=>$visitor_date_time,'gate' =>$gate,'manager_id'=>$manager_id,'cust_id'=>$user_id,'created_at'=> date('Y-m-d H:i:s'),'updated_at'=> date('Y-m-d H:i:s'));

      
          $sqlId = DB::table('visitors')
          ->where('vist_id',$visiter_id)
          ->where('cust_id',$request['user_id'])
          ->update($data);
      
        if($sqlId!=0){
                  //manger_id
                  $manager_id = $request['manager_id'];
                  // get sequerity query
                  $securitys = DB::table('security')
                  ->where('manager_id',$manager_id)
                  ->where('deleted_flag',0)
                  ->get();

                  $id = array();  
                  $mobile = array();
                  //for each used mulit array get id 
                  foreach ($securitys as $security) {
                    $id[]=$security->secu_id;
                    $mobile[]=$security->mobile;
                  }
                  // get fcm key/tokan query
                  $fcms = DB::table('fcm_customer')
                  ->whereIn('account_id',$id)
                  ->whereIn('mobile',$id)
                  ->get();
                  //emty array
                  $data = array();
                  $notification_status = false;
                  foreach ($fcms as $fcm) {
                    $fcmKey = $fcm->fcmKey;
                    $mobile = $fcm->mobile;
                    var_dump($mobile);
                    
                    $message = "Test message";
                    // push PushNotification call 
                    $app_type ='security';
                    $notification_status = PushNotification($fcmKey, $message,$mobile,$app_type);
                    
                    // set notification status 
                    if ($notification_status === 1) {
                      $notification_status = true;
                    }elseif ($notification_status === 0) {
                     $notification_status = false;
                    }

                  }
                  
                  $data = array("success" =>true,"message" => "Visiter Update Successfully",'notification_status'=>$notification_status);
                  return response()->json($data);
            }else
            {
                  $data = array("success" =>false,"message" => "Data Not Found");
                  return response()->json($data);
            }
        }else{
                $data = array("success" =>false,"message" => "Invalid Parameters");
                return response()->json($data);
        }
        
     }
     /*
      Delete Visiter
     */
     public function visitorDelete(Request $request){

      header('Content-Type: application/json');

          $visitor_id = $request['visitor_id'];
          $user_id = $request['user_id'];
          $manager_id = $request['manager_id'];

         if( isset($visitor_id) && isset($user_id) && $visitor_id !="" && $manager_id !="" && isset($manager_id))
          {    
               $visiterDelete = DB::table('visitors')
               ->where('vist_id',$request['visitor_id'])
               ->where('cust_id',$request['user_id'])
               ->update(['deleted_flag'=>'1']);

               if($visiterDelete!=0){

                    $data = array("success" =>true,"message" => "Visitor Successfully Delete");
                    return response()->json($data);

               }else{

                   $data = array("success" =>false,"message" => "Data Not Found");
                   return response()->json($data);
               }

          }else{
                $data = array("success" =>false,"message" => "Invalid Parameters");
                return response()->json($data);
        }
     }  
     /*
     visiter List
    */
     public function visitorslist(Request $request){

        header('Content-Type: application/json');
        $user_id = $request['user_id'];
        $manager_id = $request['manager_id'];


        if( isset($user_id) && $user_id !="" && isset($manager_id) && $manager_id !="")
        {
            // get visiter 
            $visiters = DB::table('visitors')
            ->where('deleted_flag',0)
            ->where('cust_id',$user_id)
            ->where('manager_id',$manager_id)
            ->get();

            //emty array
            $data = array();
            foreach ($visiters as $value) {
            
                $type_id =  $value->type_id;
                $VisitorType = DB::table('visitor_type')->where('id',$type_id)->first();


                if (!isset($visiters)) {
                  $visitor_type_logo =null;
                  $visitor_type =null;
                }else{
                  $visitor_type_logo =$VisitorType->logo;
                  $visitor_type =$VisitorType->type;
                }
               

                $compa_id =  $value->comp_id;
                $company = DB::table('visitor_company')->where('id',$compa_id)->first(); 

                if (!isset($company)) {
                  $company_logo =null;
                  $company_name =null;
                }else{
                  $company_logo =$company->company_logo;
                  $company_name =$company->company_name;
                }
                //otp generated
                //$otp = mt_rand(1000,9999);

               $data[] = array('visitor_id' => $value->vist_id,
                               'comp_id'=>$value->comp_id,
                               'visitor_type_id' => $value->type_id,
                               'visitor_type' => $visitor_type,
                               'visitor_type_logo'=>$visitor_type_logo,
                               'visitor_name'=>$value->visitor_name,
                               'company_logo'=>$company_logo,
                               'company_name'=>$company_name,
                               'vehicle_no'=>$value->vehicle_no,
                               'visitor_mobile'=>$value->visitor_mobile,
                               'gate'=>$value->gate,
                               'expriy_date_time'=>$value->expriy_date_time,
                               'otp'=>$value->otp,
                               'status'=>$value->status,
                               'visitor_time'=>$value->date_time,
                               'created_at'=>$value->created_at
                             );
            }
            if($data > 0){
                 $data = array("success" =>true,'data'=>$data,"message" => "Record Fetch Successfully",);
                  return response()->json($data);
            }else{

               $data = array("success" =>false,'data'=>null,"message" => "Data Not Found!",);
                return response()->json($data);
            }
        }else{

            $data = array("success" =>false,'data'=>null,"message" => "Invalid Parameters",);
            return response()->json($data);
        }
     }


}

