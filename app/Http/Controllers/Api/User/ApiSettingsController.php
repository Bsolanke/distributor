<?php

namespace Distributor\Http\Controllers\Api\user;

use Illuminate\Http\Request;
use Distributor\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Hash;

class ApiSettingsController extends Controller
{
    /*
      change password api
    */
    public function changePassword(Request $request)
    {
    	header('Content-Type: application/json');

    	 $old_password = $request['old_password'];
    	 $new_password = $request['new_password'];
    	 $user_id = $request['user_id'];

	     if( isset($old_password) && $old_password !="" && isset($new_password) && $new_password !="" && isset($user_id) && $user_id!="" )
	      {
	      	//DB::enableQueryLog();
	    	$user=DB::table('customers')
	             ->where('cust_id','=',$user_id)
	             ->first();
	         // check old password 
		    if(!Hash::check($request['old_password'], $user->password)){

			        $data = array("success" =>false,"message" => "Wrong password entered.",);
                    return response()->json($data);

			  }else{

				  	$data = array('password' => Hash::make($new_password),'updated_at'=>date('Y-m-d H:i:s'));
				  	// update password
				  	$PassUpdate=DB::table('customers')
		             ->where('cust_id','=',$user_id)
		             ->update($data);
		            if ($PassUpdate >0) {

			              $dataMessage = array("success" =>true,"message" => "Your password has been updated.",);
		                  return response()->json($dataMessage);
		            }else{

		            	  $dataMessage = array("success" =>false,"message" => "Data Not Found.",);
		                  return response()->json($dataMessage);
		            }   
			    }
	       }else{

	       	$dataMessage = array("success" =>false,"message" => "Invalid Parameters");
		    return response()->json($dataMessage);
	       }
    }
    /*
      notificational Allow On Off api
    */
    public function notificationAllow(Request $request)
    {   header('Content-Type: application/json');
        
       
      	$user_id = $request['user_id'];
      	$notification_is =$request['notification_is'];
      	
      	if (isset($user_id) && $user_id !="" && isset($notification_is)) {
      		
      		if ($notification_is == true) {
      			// update on 
      			$user=DB::table('customers')
	             ->where('cust_id','=',$user_id)
	             ->update(['features_allowed'=>NOTIFICATION_ON]);
	     	    //message 
	            $dataMessage = array("success" =>true,"message" => "Notifications On Successfully");

      		}elseif ($notification_is == false) {
      			//updated off
      			$user=DB::table('customers')
	             ->where('cust_id','=',$user_id)
	             ->update(['features_allowed'=>NOTIFICATION_OFF]);
	    		//message 
	            $dataMessage = array("success" =>true,"message" => "Notifications Off Successfully");
      		}
      		if (count($user) >0) {
      			return response()->json($dataMessage);
      		}else{
      			$dataMessage = array("success" =>false,"message" => "Data Not Found");
		        return response()->json($dataMessage);
      		}

      	}else{
      	    $dataMessage = array("success" =>false,"message" => "Invalid Parameters");
		    return response()->json($dataMessage);	
      	}
    }
    /*
	  Get settings user
    */
    public function GetSettings(Request $request)
    {   
    	header('Content-Type: application/json');
        
      	$user_id = $request['user_id'];
      	$manager_id = $request['manager_id'];

      	if (isset($user_id) && $user_id !="" && isset($manager_id) && $manager_id !="") {
      			
      			// get customer info
      			$value=DB::table('customers')
	             ->where('cust_id','=',$user_id)
	             ->where('manager_id','=',$manager_id)
	             ->first();

	        $notification = true; // default
	        if ( $value->features_allowed ===  NOTIFICATION_ON) // notification on
	        {
	        	$notification = true;
	        }elseif ($value->features_allowed ===  NOTIFICATION_OFF) // notification off
	        {
	        	$notification = false;
	        }else
	        {
	        	$notification = true; //default 
	        }

	        $data = array('notification' => $notification);

      		if (count($value) >0) 
      		{
			    $dataMessage = array("success" =>true,'data'=>$data,"message" => "Record Fetch Successfully");
		        return response()->json($dataMessage);	
      		}else{
      			$dataMessage = array("success" =>false,"message" => "Data Not Found");
		        return response()->json($dataMessage);
      		}

      	}else{
      	    $dataMessage = array("success" =>false,"message" => "Invalid Parameters");
		    return response()->json($dataMessage);	
      	}
    }

}

