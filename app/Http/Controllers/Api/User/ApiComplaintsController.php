<?php

namespace Distributor\Http\Controllers\Api\user;

use Illuminate\Http\Request;
use Distributor\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Hash;

class ApiComplaintsController extends Controller
{

    public function complaintsType(Request $request)
    {

       header('Content-Type: application/json');

       $user_id = $request['user_id'];
       $manager_id = $request['manager_id'];

       if($user_id !="" && isset($user_id) && $manager_id !="" && isset($manager_id)) 
       {
            $complaintsType = DB::table('complaints_type')
                              ->where('manager_id',$manager_id)
                              ->where('deleted_flag',0)
                              ->get();

            foreach ($complaintsType as $value) {

              $data[] = array('id'=>$value->id,
                            'complaints_type' => $value->complaints_type,
                            'manager_id'=>$value->manager_id 
                           );
            }

            if (count($complaintsType)>0) {
                  $data = array("success" =>true,'data'=>$data,"message" => "Record Fetch Successfully",);
                  return response()->json($data);
            }else{
                  $data = array("success" =>false,"message" => "Data Not Found!",);
                  return response()->json($data);
            }
       }else{
             $data = array("success" =>false,"message" => "Invalid Parameters",);
              return response()->json($data);
       }
      
        
    }

    /*
      Add / create Complaint api
    */    
    public function createComplaint(Request $request){

        $user_id = $request['user_id'];
        $complaint_type = $request['complaint_type'];
        $complaint_description = $request['complaint_description'];
        $manager_id = $request['manager_id'];

        if($user_id !="" && isset($user_id) && $manager_id !="" && isset($manager_id) && isset($complaint_description) && $complaint_type !="") 
        {
              
              $data = array('complaints_type_id' => $complaint_type,'complaints_description'=> $complaint_description,'cust_id'=>$user_id,'manager_id'=>$manager_id,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'));

              $sql = DB::table('complaints')->insert($data);
              $lastId = DB::getPdo()->lastInsertId();

              if (isset($lastId)) {
                    // send notification to admin
                    $user = DB::table('users')->where('id',$manager_id)->first();
                    // get fcm key 
                    $get_fcm_key = DB::table('fcm_customer')->where('account_id',$user->id)->where('mobile',$user->mobile)->first();
                    if ($get_fcm_key >0) {
                       
                        $fcmKey = $get_fcm_key->fcmKey;
                        $mobile = $get_fcm_key->mobile;
                        $message = "New Complaint Added";
                         // push PushNotification call 
                        $app_type ='user';
                        $notification_status = PushNotification($fcmKey, $message,$mobile,$app_type);
                        // set notification status 
                        if ($notification_status === 1) {
                          $notification_status = true;
                        }elseif ($notification_status === 0) {
                         $notification_status = false;
                        }

                        $data = array("success" =>true,"message" => "Complaint Add Successfully.",'notification_status'=>$notification_status);
                        return response()->json($data);
                }else{
                        $data = array("success" =>true,"message" => "Complaint Add Successfully.",'notification_status'=>false);
                        return response()->json($data);
                }
              }else{

                   $data = array("success" =>false,"message" => "Data Not Found",);
                   return response()->json($data);
              }

         }else{
              $data = array("success" =>false,"message" => "Invalid Parameters",);
              return response()->json($data);
        }
    }
    /*
      Complaint List
    */
     public function ComplaintsList(Request $request){

        $user_id = $request['user_id'];
        $manager_id = $request['manager_id'];

        if($user_id !="" && isset($user_id) && $manager_id !="" && isset($manager_id)) 
        {   
            $complaints_data = DB::table('complaints') // get current user complainte list
            ->where('cust_id',$user_id)
            ->where('manager_id',$manager_id)
            ->where('deleted_flag',0)
            ->get();
            
            if (count($complaints_data)>0) {
              
              foreach ($complaints_data as $value) {

                $complaint_type = DB::table('complaints_type')->where('deleted_flag',0)->where('id',$value->complaints_type_id)->first();

                $data[] = array('comp_id' =>$value->comp_id ,'comp_type'=>$value->complaints_type,'complaint_desc'=>$value->complaints_description,'status'=>$value->status,'features_allowed'=>$value->features_allowed,'created_at'=>$value->created_at,'updated_at'=>$value->updated_at );
              }
                $data = array("success" =>true,'data'=>$data,"message" => "Record Fetch Successfully",);
                return response()->json($data);
              
            }else{

              $data = array("success" =>false,'data'=>null,"message" => "Data Not Found!",);
                return response()->json($data);

            }
              
        }else{
              $data = array("success" =>false,"message" => "Invalid Parameters",);
              return response()->json($data);
        }
    }
    

}

