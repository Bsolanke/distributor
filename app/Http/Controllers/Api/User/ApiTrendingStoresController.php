<?php

namespace Distributor\Http\Controllers\Api\user;

use Illuminate\Http\Request;
use Distributor\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;

class ApiTrendingStoresController extends Controller
{
    /*
      create/add Trending Stores
    */
    public function createdTrendingStores(Request $request)
    {
          header('Content-Type: application/json');
          $trending_title = $request['trending_title'];
          $trending_description = $request['trending_description'];
          $user_id = $request['user_id'];
          $manager_id = $request['manager_id'];

          // validation required and images format
          $validation = Validator::make($request->all(), [
          'trending_images.*' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
          'trending_title'=>'required',
          'user_id'=>'required',
          'manager_id'=>'required'         
          ]);

	       if($validation->passes()) //valdation true
	       {
	       
			  $data = array('trending_title' => $trending_title,
			    			'trending_description'=>$trending_description,
			    			'cust_id' =>$user_id,
			    			'manager_id' =>$manager_id,
			    			'created_at'=>date('Y-m-d H:i:s'),
			    			'updated_at'=>date('Y-m-d H:i:s')
			    			);

			    $insertId= DB::table('trending_stores')->insertGetId($data);

			    if (count($insertId) >0) {

			   if($request->hasfile('trending_images'))
		         {
		         	
		            foreach($request->file('trending_images') as $image)
		            {
		                $new_name = rand() . '.' . $image->getClientOriginalExtension();
	         			$image->move(public_path('upload/trendingImages/'), $new_name);
	          			$path = 'upload/trendingImages/'.$new_name;

	          			$insertSql= DB::table('trending_images')->insert(['trending_id'=>$insertId,'trending_images'=>$path,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]); // trending stores image same other table trending_images
		            }

			       $dataMessage = array("success" =>true,"message" => "Trending Stores Add Successfully!",);
				   return response()->json($dataMessage);

			    }else{

			    	$dataMessage = array("success" =>false,"message" => "Data Not Found!",);
		            return response()->json($dataMessage);
			    }
		     	  
	     	}
	     	  
	    }else{
	         $dataMesaage = array('status'=>false,'message'=>'Invalid Parameters','required' => $validation->errors()->all());
	          return response()->json($dataMesaage);
	    }
    }
    /*
	  Trending Stores List
    */
	 public function trendingStoresList(Request $request)
     {
          header('Content-Type: application/json');

          $user_id = $request['user_id'];
          $manager_id = $request['manager_id'];

          // validation required and images format
          $validation = Validator::make($request->all(), [
          'user_id'=>'required',
          'manager_id'=>'required'         
          ]);
	       if($validation->passes()) //valdation true
	       {
	       
			   $trending=DB::table('trending_stores as t')
			   ->leftJoin('customers as c', 't.cust_id', '=', 'c.cust_id')
			   ->rightjoin('users as u', 'c.manager_id', '=', 'u.id')
			   ->select('t.*','c.customer_name','c.profile_img','c.flat_no','c.building','c.mobile','u.society_name')
	           ->where('t.deleted_flag',0)
	           ->where('t.manager_id','=',$manager_id)
	           ->get();

			    foreach ($trending as $value) {
			    	
			    		$trending_images = DB::table('trending_images')
			    		->select('trending_images','id')
			    		->where('trending_id',$value->trending_id)
			    		->where('deleted_flag',0)
			    		->get();
			    		$images = array();
			    		// get trending images
			    		foreach ( $trending_images as $key => $value1 )
						{
							$images[] =$key=$value1->trending_images;
						}

		        		$data[] = array('trending_id' => $value->trending_id,
		        						'trending_title' => $value->trending_title,
						    			'trending_description'=>$value->trending_description,
						    			'customer_name'=>$value->customer_name,
						    			'mobile'=>$value->mobile,
						    			'flat_no'=>$value->flat_no,
						    			'building'=>$value->building,
						    			'society_name'=>$value->society_name,
						    			'profile_img'=>$value->profile_img,
						    			'trending_images' =>$images,
						    			'created_at'=>$value->created_at,
						    			'updated_at'=>$value->updated_at
					    			   );
			    }
			    if (count($trending) >0) {

			       $dataMessage = array("success" =>true,'data'=>$data,"message" => "Record Fetch Successfully",);
				   return response()->json($dataMessage);
			     	  
		     	}else{
			    	$dataMessage = array("success" =>false,"message" => "Data Not Found!",);
		            return response()->json($dataMessage);
			    }
	     	  
		    }else{
		         $dataMesaage = array('status'=>false,'message'=>'Invalid Parameters','required' => $validation->errors()->all());
		          return response()->json($dataMesaage);
		    }
    }
    /*
		Trending Stores Delete
    */
     public function trendingStoresDelete(Request $request)
     {
          header('Content-Type: application/json');

          $user_id = $request['user_id'];
          $manager_id = $request['manager_id'];
 		  $trending_id = $request['trending_id'];

          // validation required and images format
          $validation = Validator::make($request->all(), [
          'user_id'=>'required',
          'manager_id'=>'required',
          'trending_id'=>'required'         
          ]);
	       if($validation->passes()) //valdation true
	       {
	           //DB::connection()->enableQueryLog();
			   $trending=DB::table('trending_stores')
	           ->where('trending_id',$trending_id)
	           ->update(['deleted_flag'=>'1']);
	           // trending images 
	           $trending_images=DB::table('trending_images')
	           ->where('trending_id',$trending_id)
	           ->update(['deleted_flag'=>'1']);

	           //dd(DB::getQueryLog());die();

			    if (count($trending)) {

			       $dataMessage = array("success" =>true,"message" => "Trending Stores Delete Successfully",);
				   return response()->json($dataMessage);
			     	  
		     	}else{
			    	$dataMessage = array("success" =>false,"message" => "Data Not Found!",);
		            return response()->json($dataMessage);
			    }
	     	  
		    }else{
		         $dataMesaage = array('status'=>false,'message'=>'Invalid Parameters','required' => $validation->errors()->all());
		          return response()->json($dataMesaage);
		    }
    }
}

