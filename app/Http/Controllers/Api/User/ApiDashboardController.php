<?php

namespace Distributor\Http\Controllers\Api\user;

use Illuminate\Http\Request;
use Distributor\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;

class ApiDashboardController extends Controller
{
   
    /*
	  getDashboard Data
    */
	 public function getDashboard(Request $request)
     {
          header('Content-Type: application/json');

          $user_id = $request['user_id'];
          $manager_id = $request['manager_id'];

          // validation required and images format
          $validation = Validator::make($request->all(), [
          'user_id'=>'required',
          'manager_id'=>'required'         
          ]);
	       if($validation->passes()) //valdation true
	       {
	            // get trendign stores list
	       	    $trending = array();
				$trending = $this->trendingStoresData($manager_id,$user_id);  

				// myadd list
				$myadds = array(); 
				$trending = $this->trendingStoresData($manager_id,$user_id); 

				$data = array('trending' => $trending, 'myadds'=>$trending); 

				if(count($trending) >0 || count($myadds) >0) {
					$dataMessage = array("success" =>true,'data'=>$data,"message" => "Record Fetch Successfully",);
				    return response()->json($dataMessage);
				}else{
				   $dataMessage = array("success" =>false,'data'=>null,"message" => "Data Not Found!",);
				   return response()->json($dataMessage);
				}
	     	  
		    }else{
		         $dataMesaage = array('status'=>false,'message'=>'Invalid Parameters','required' => $validation->errors()->all());
		          return response()->json($dataMesaage);
		    }
    }

    /*
	  Get trending Stores
    */
	 public function trendingStoresData($manager_id,$user_id)
     {
     	// $trending=DB::table('trending_stores')
	     //       ->where('manager_id','=',$manager_id)
	     //       ->where('deleted_flag',0)
	     //       ->orderBy('updated_at','DESC')
	     //       ->limit(2) // limit 2
	     //       ->get();

	    $trending=DB::table('trending_stores as t')
			   ->leftJoin('customers as c', 't.cust_id', '=', 'c.cust_id')
			   ->rightjoin('users as u', 'c.manager_id', '=', 'u.id')
			   ->select('t.*','c.customer_name','c.profile_img','c.flat_no','c.building','c.mobile','u.society_name')
	           ->where('t.deleted_flag',0)
	           ->where('t.manager_id','=',$manager_id)
	           ->orderBy('t.updated_at','DESC')
	           ->limit(2) // limit 2
	           ->get();

	           $trendingData = array(); 
			    foreach ($trending as $value) 
			    {
			    		
		    		$trending_images = DB::table('trending_images')
		    		->select('trending_images','id')
		    		->where('trending_id',$value->trending_id)
		    		->where('deleted_flag',0)
		    		->orderBy('updated_at','DESC')
		    		->get();

		    		$images = array();
		    		// get trending images
		    		foreach ( $trending_images as $key => $value1 )
					{
						$images[] =$key=$value1->trending_images;
					}

	        		$trendingData[] = array('trending_id' => $value->trending_id,
			        						'trending_title' => $value->trending_title,
							    			'trending_description'=>$value->trending_description,
							    			'customer_name'=>$value->customer_name,
							    			'mobile'=>$value->mobile,
							    			'flat_no'=>$value->flat_no,
							    			'building'=>$value->building,
							    			'society_name'=>$value->society_name,
							    			'profile_img'=>$value->profile_img,
							    			'trending_images' =>$images,
							    			'created_at'=>$value->created_at,
							    			'updated_at'=>$value->updated_at
							    			);
			    }
				return $trendingData;
     }

    /*
	  Get myAddsList / advantisment 
    */
	 public function myaddsList($manager_id,$user_id)
     {
     	 header('Content-Type: application/json');
          
              $advantisment = DB::table('advantisment')
              ->where('deleted_flag',0)
              ->where('manager_id',$manager_id)
              ->orderBy('updated_at','DESC')
	          ->limit(2) // limit 2
              ->get();
              	$myadds = array(); //arr de
                foreach ($advantisment as $value) {
                  
                  $myadds[] = array('advantisment_id' =>$value->advantisment_id ,
                                  'advantisment_title' =>$value->advantisment_title,
                                  'stores'=>$value->stores,
                                  'advantisment_description'=>$value->advantisment_description,
                                  'advantisment_images'=>$value->advantisment_images,
                                  'created_at'=>$value->created_at,
                                  'updated_at'=>$value->updated_at
                                );
                }
                return $myadds;
        
     }
    
}

