<?php

namespace Distributor\Http\Controllers\Api\user;

use Illuminate\Http\Request;
use Distributor\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;

class ApiMyAddController extends Controller
{
    /*
      My Add List api
    */
    public function myAddsList(Request $request)
    {
          header('Content-Type: application/json');
          $manager_id = $request['manager_id'];
          if (isset($manager_id) && $manager_id !="") 
          {
              $advantisment = DB::table('advantisment')
              ->where('deleted_flag',0)
              ->where('manager_id',$manager_id)
              ->get();
              if (count($advantisment)>0) {
                foreach ($advantisment as $value) {
                  
                  $data[] = array('advantisment_id' =>$value->advantisment_id ,
                                'advantisment_title' =>$value->advantisment_title,
                                'stores'=>$value->stores,
                                'advantisment_description'=>$value->advantisment_description,
                                'advantisment_images'=>$value->advantisment_images,
                                'created_at'=>$value->created_at,
                                'updated_at'=>$value->updated_at
                              );
                }
                  $dataMessage = array("success" =>true,'data'=>$data,"message" => "Record Fetch Successfully",);
                  return response()->json($dataMessage);
              }else{
                  $dataMessage = array("success" =>false,'data'=>null,"message" => "Data Not Found!",);
                  return response()->json($dataMessage);
              }
          }else{
            $dataMessage = array("success" =>false,"message" => "Invalid Parameters",);
            return response()->json($dataMessage);
          }
    }

}
