<?php

namespace Distributor\Http\Controllers\Api\user;

use Illuminate\Http\Request;
use Distributor\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Hash;

class ApiVehiclesController extends Controller
{
    /*
      Vehicle List api
    */
    public function vehiclesList(Request $request)
    {   header('Content-Type: application/json');
       
      	$user_id = $request['user_id'];
      	$manager_id =$request['manager_id'];

      	if (isset($user_id) && $user_id !="" && isset($manager_id) &&  $manager_id !="" ) {
      		
      		$vehicleSql = DB::table('vehicles')
      		->where('cust_id',$user_id)
      		->where('manager_id',$manager_id)
          ->where('deleted_flag',0)
      		->get();
            
            foreach ($vehicleSql as $vehicle) {
            	$data[]= array('vehicle_id' => $vehicle->vehi_id,'vehicle_no' => $vehicle->vehicle_no,'vehicle_name' => $vehicle->vehicle_name,'vehicle_type' => $vehicle->vehicle_type,'created_at' => $vehicle->created_at,'updated_at' => $vehicle->updated_at );
            }
            if (count($vehicleSql) >0 ) {
            	$data = array("success" =>true,'data'=>$data,"message" => "Record Fetch Successfully",);
                return response()->json($data);
            }else{
            	$data = array("success" =>false,'data'=>null,"message" => "Data not Found!",);
                return response()->json($data);
            }
      		
      	}else{
      	    $dataMessage = array("success" =>false,"message" => "Invalid Parameters");
		    return response()->json($dataMessage);	
      	}
    }
    /*
	  add/ create Vehicle 
    */
    public function createVehicle(Request $request)
    {   header('Content-Type: application/json');
        
      	$user_id = $request['user_id'];
      	$manager_id = $request['manager_id'];
      	$vehicle_no = $request['vehicle_no'];
      	$vehicle_name = $request['vehicle_name'];
      	$vehicle_type = $request['vehicle_type'];

      	if (isset($user_id) && $user_id !="" && isset($manager_id) && $manager_id !="" && isset($vehicle_no) && $vehicle_no !="" && isset($vehicle_type) && $vehicle_type !="") {
      		
      	     $data = array('vehicle_no' => $vehicle_no,'vehicle_type'=>$vehicle_type,'vehicle_name'=>$vehicle_name,'cust_id'=>$user_id,'manager_id'=>$manager_id,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'));	

	      	     $insertId = DB::table('vehicles') // insert vehicle query
	      	     ->insertGetId($data); // get insert id

	      	if (count($insertId) >0) {
	      		 $dataMessage = array("success" =>true,"message" => "Vehicle Add Successfully");
		    	 return response()->json($dataMessage);	
	      	}else{
	      		 $dataMessage = array("success" =>false,"message" => "Data Not Found!");
		    	 return response()->json($dataMessage);	
	      	}

      	}else{
      	    $dataMessage = array("success" =>false,"message" => "Invalid Parameters");
		        return response()->json($dataMessage);	
      	}
    }
    /*
      Delete Vehicle
    */
    public function deleteVehicle(Request $request){

      header('Content-Type: application/json');
      
      $user_id = $request['user_id'];
      $vehicle_id = $request['vehicle_id'];

      if (isset($user_id) && $user_id !="" && isset($vehicle_id) && $vehicle_id !="") {
          $deleteVehicle = DB::table('vehicles')
                          ->where('cust_id',$user_id)
                          ->where('vehi_id',$vehicle_id)
                          ->update(['deleted_flag'=>'1']);

          if (count($deleteVehicle) >0) {
              $dataMessage = array("success" =>true,"message" => "Vehicle Successfully Delete");
              return response()->json($dataMessage);
          }else{
              $dataMessage = array("success" =>false,"message" => "Data Not Found!");
              return response()->json($dataMessage);
          }
      }else{
          $dataMessage = array("success" =>false,"message" => "Invalid Parameters");
          return response()->json($dataMessage);  
      }

    }
}

