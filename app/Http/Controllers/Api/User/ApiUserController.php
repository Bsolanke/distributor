<?php

namespace Distributor\Http\Controllers\Api\user;

use Illuminate\Http\Request;
use Distributor\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;

class ApiUserController extends Controller
{
    /*
      login user api
    */
    public function loginUser(Request $request)
    {
      header('Content-Type: application/json');

      //logout params
      if (isset($request['isLogout'])) {
        $isLogout = $request['isLogout'];
      }

      if( isset($request['mobile']) && $request['mobile']!="")
      {
             $MobileExist=DB::table('customers')  // check mobile no exist
             ->where('mobile','=',$request['mobile'])
             ->where('deleted_flag',0)
             ->get();
  
             if (count($MobileExist)>0) 
             {
               //$otp = mt_rand(1000,9999);
               //static otp
               $otp = 1234;
               // save otp 
               $dataOtp = array('otp' => $otp, 'mobile' =>$request['mobile']);
               //$save=ApiUserModel::saveOtp($dataOtp);
               DB::table('saveotp')->insert($dataOtp);
               $lastId = DB::getPdo()->lastInsertId();
               if(isset($lastId))
               {
         	         $data = array(
            			  "success" =>true,
            			  "otp"=>$otp,
            			  "message" => "Send Otp Successfully",
            			);
          			return response()->json($data);
               }
               else
               {
                  $data = array(
              		"success" =>false,
              		"message" => "Data Not Found",
             			 );
        			    return response()->json($data);
        		   }
             }else
             {
                $data = array(
                "success" =>false,
                "message" => "Mobile no is not exist",
                 );
                return response()->json($data);
             }
          
      }elseif ( $isLogout == 1 ) 
      {
            //user logged out
            $rows = DB::table('fcm_customer')->where('fcmKey', $fcmKey)->where('mobile', $mobile)->delete();    
      }else
      {
          $data = array(
      		"success" =>false,
      		"message" => "All Data Required",
     			 );
    			return response()->json($data);
       }	
     }

     /*
       otp verifyOtp user
     */
     public function verifyOtp(Request $request){
      header('Content-Type: application/json');

      if( isset($request['mobile']) && $request['mobile']!="" && isset($request['otp']) && $request['otp']!="")
      {
        //check otp 
        $check_otp=DB::table('saveotp')
        ->where('mobile','=',$request['mobile'])
        ->where('otp','=',$request['otp'])
        ->get();
  
        if (count($check_otp)>0) {
          // verify after delete entry
          DB::table('saveotp')->where('mobile','=',$request['mobile'])
          ->where('otp','=',$request['otp'])->delete();
          //user data 
          $UserData=DB::table('customers as c')
          ->leftJoin('users as u', 'c.manager_id', '=', 'u.id')
          ->select('c.*','u.society_name')
          ->where('c.mobile','=',$request['mobile'])
          ->get();
         
         $visitorTypeData =DB::table('visitor_type') // visitor types
         ->where('deleted_flag',0)
         ->get();
         $visitorTypeList = array();
         
         foreach ($visitorTypeData as $key => $visitorType) {
           $visitorTypeList[] = array('id' =>$visitorType->id ,'type' =>$visitorType->type,'logo' =>$visitorType->logo );
         }

          $data = array(
            "success" =>true,
            "data"=>$UserData,
            "visitorType"=>$visitorTypeList,
            "message" => "login Successfully",
             );
          return response()->json($data);
        }else
        {
          $data = array(
            "success" =>false,
            "data"=>null,
            "message" => "Invalid Otp",
             );
          return response()->json($data);
        }
      }else
      {
        $data = array(
        "success" =>false,
        "message" => "All Data Required",
         );
        return response()->json($data);
      }

     }

    /* 
     FCM Register/Update of User
    */
    public function fcm_register(Request $request) {
    
        header('Content-Type: application/json');

        $email = $request['email'];
        $account_id = $request['account_id'];
        $password_token = $request['password_token'];

        $unique_phone_id = $request['unique_phone_id'];
        $fcm_server_id = $request['fcm_server_id'];
        $device_details = $request['device_details'];

        $phone_type = $request['phone_type'];
        $fcmKey = $request['fcmKey'];

        $mobile = $request['mobile'];
        
        if ( empty($mobile) || empty($account_id) || empty($password_token) ||  empty($fcmKey) || empty($phone_type) ) {
            
            return json_encode( array('error' => true, 'message'=>'Invalid Parameters', 'mobile' => $mobile,'id' => $account_id, 'password_token' => $password_token) );
        }
       //Verify user
       $user = $this->verifyUser($account_id, $mobile);
        // if no user, send error    
        if (  empty($user) && count($user) < 1 )   {
          //for API call, 
          //return invalid credentails error for user not found in db or invalid password or email all cases
          return json_encode(array('success' => false, 'message'=>'Invalid Credentials', 'email' => $email ));
        }
        //Always try to update first on the basis of device phone id if its failed then insert new registration id
        if ( !empty($unique_phone_id)  ) {

          $message = "Welcome To The LiveGate App";
          // push PushNotification call 
          $app_type ='user';
          $notification_status = PushNotification($fcmKey, $message,$mobile,$app_type);

          // set notification status 
          if ($notification_status === 1) {
            $notification_status = true;
          }elseif ($notification_status === 0) {
           $notification_status = false;
          }
          ///var_dump($user['']);die();
          $affectedRows = DB::table('fcm_customer')
            //->where('unique_phone_id', $unique_phone_id)
            ->where('mobile', $mobile)
            ->update(['fcmKey' => $fcmKey, 'device_details' => $device_details, 'updated_at' => date('Y-m-d H:i:s')]);
            
            if ( $affectedRows > 0 ) {

                return json_encode(array('success' => true, 'message' => 'Notification key updated!', 'fcm_server_id' => 0, 'notification_status'=>$notification_status ));

            }else{

                $id = DB::table('fcm_customer')->insertGetId(
                    ['email' => $email, 'mobile'=> $mobile, 'account_id' => $account_id, 'unique_phone_id' => $unique_phone_id, 'fcmKey' =>$fcmKey, 'phone_type' => $phone_type, 'device_details' => $device_details,'pushnotificationios'=>$message, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s') ]
                );

                return json_encode(array('success' => true, 'message' => 'Notification key created.', 'fcm_server_id' => $id,'notification_status'=>$notification_status ));    
            }

        }else{

            return json_encode(array('success' => false, 'message' => 'Empty phone id', 'fcm_server_id' => 0 ));    
        }
   }

    /*
     Verify API user
    */
    function verifyUser($account_id, $mobile)
    {
      //verify user only
        //Cache query for 24 hours
        $user = DB::table('customers')->where('cust_id',$account_id)->where('mobile', $mobile)->first();
        return $user;
    }
    
     /******** Noticeboard **********/

    /*
      Noticeboard api society admin add notice all member display 
    */
    public function noticeboard(Request $request){

      header('Content-Type: application/json');

      if (isset($request['user_id']) && isset($request['manager_id']) && $request['user_id'] !="" && $request['manager_id'] !="") 
      {
         # code...
          $cust_id = $request['user_id']; // user_id as cust_id
          $manager_id = $request['manager_id']; // society id
    
          $current_date = date('Y-m-d H:i:s'); // current date 

          $noticeSql = array(); // array define

          $noticeSql = DB::table('noticeboard')->
          where('manager_id',$manager_id)
          //->where('expriy_date','>',$current_date)
          ->get();
          
          //for 
          foreach ($noticeSql as $notice) {
                
                $created_at = date("Y-m-d", strtotime($notice->created_at));
                
                $ex_flag = null;// flag define
                if ($current_date < $notice->expriy_date)
                {
                      $ex_flag = 0;  
                }else{
                      $ex_flag = 1;
                }

               $data[] = array('noti_id' => $notice->noti_id,
                               'title' => $notice->title,
                               'notice' => $notice->notice,
                               'expriy_date' => date("Y-m-d", strtotime($notice->expriy_date)),
                               'manager_id' => $notice->manager_id,
                               'is_expiry'=>$ex_flag,
                               'deleted_flag'=>$notice->deleted_flag,
                               'created_at'=>$created_at,
                               'updated_at'=>$notice->updated_at);   
          }  
          if (count($noticeSql) >0 ) {

              $data = array("success" =>true,'data'=>$data,"message" => "Record Fetch Successfully",);
              return response()->json($data);

           }else{

                 $data = array("success" =>false,'data'=>null,"message" => "Data Not Found!",);
                 return response()->json($data);
           }
         }else{

              $data = array("success" =>false,'data'=>null,"message" => "Invalid Parameters",);
              return response()->json($data);
         }
    }
    /**********  Members  **************/
    /*
     all member list in society
    */
     public function allMembers(Request $request){

       header('Content-Type: application/json');

        $user_id = $request['user_id'];
        $manager_id = $request['manager_id'];
      if($user_id !="" && $manager_id !="" && isset($user_id) && isset($manager_id)) {
          
          // get manager id in all member 
          $SqlMember = DB::table('customers')
          ->where('manager_id',$manager_id)
          ->get();  

          $data = array(); // array define

          foreach ($SqlMember as $row) {

            $data[] = array('member name' => $row->customer_name,'mobile'=>$row->mobile,'email'=>$row->email,'members'=>$row->members,'profile img'=>$row->profile_img,'building'=>$row->building,'flat no'=>$row->flat_no,'flat type'=>$row->flat_type,'owner'=>$row->owner );
          }
          if (count($SqlMember) > 0) {

              $data = array("success" =>true,'data'=>$data,"message" => "Record Fetch Successfully",);
                return response()->json($data);

          }else{

            $data = array("success" =>false,'data'=>null,"message" => "Data Not Found",);
                return response()->json($data);
          }

      }else{

           $data = array("success" =>false,"message" => "Invalid Parameters",);
              return response()->json($data);
      }

    }




}
