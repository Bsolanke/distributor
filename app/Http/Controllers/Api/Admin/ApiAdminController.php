<?php

namespace Distributor\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use Distributor\Http\Controllers\Controller;
use Distributor\AdminModel\ApiAdminModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ApiAdminController extends Controller
{
    ////user login
    public function loginUser(Request $request)
    {
      header('Content-Type: application/json');

      //logout params
      if (isset($request['isLogout'])) {
        $isLogout = $request['isLogout'];
      }
      if( isset($request['mobile']) && $request['mobile']!="")
      {
             // check mobile no exist
             $MobileExist=DB::table('users')
             ->where('mobile','=',$request['mobile'])
             ->get();
  
             if (count($MobileExist)>0) 
             {
               $otp = 1234; //static 
               //$otp = mt_rand(100000,999999); /randam 
               // save otp 
               $dataOtp = array('otp' => $otp, 'mobile' =>$request['mobile']);
               //$save=ApiUserModel::saveOtp($dataOtp);
               DB::table('saveotp')->insert($dataOtp);
               $lastId = DB::getPdo()->lastInsertId();
               if(isset($lastId))
               {
         	         $data = array(
            			  "success" =>true,
            			  "otp"=>$otp,
            			  "message" => "Send Otp Successfully",
            			);
          			return response()->json($data);
               }
               else
               {
                  $data = array(
              		"status" =>false,
              		"message" => "Data Not Found",
             			 );
        			    return response()->json($data);
        		   }
             }else
             {
                $data = array(
                "status" =>false,
                "message" => "Mobile No Not Exist",
                 );
                return response()->json($data);
             }
          
      }elseif ( $isLogout == 1 ) 
      {
            //user logged out
            $rows = DB::table('fcm_customer')->where('fcmKey', $fcmKey)->where('mobile', $mobile)->delete();    
      }else
      {
          $data = array(
      		"status" =>false,
      		"message" => "All Data Required",
     			 );
    			return response()->json($data);
       }	
     }

     //otp verifyOtp
     public function verifyOtp(Request $request){
      header('Content-Type: application/json');

      if( isset($request['mobile']) && $request['mobile']!="" && isset($request['otp']) && $request['otp']!="")
      {
        //check otp 
        $check_otp=DB::table('saveotp')
        ->where('mobile','=',$request['mobile'])
        ->where('otp','=',$request['otp'])
        ->get();
  
        if (count($check_otp)>0) {
        	// verify after delete entry
	        DB::table('saveotp')->where('mobile','=',$request['mobile'])
	        ->where('otp','=',$request['otp'])->delete();

          //user data 
          $UserData=DB::table('users')
          ->where('mobile','=',$request['mobile'])
          ->get();

          $data = array(
            "success" =>true,
            "data"=>$UserData,
            "message" => "login Successfully",
             );
          return response()->json($data);
        }else
        {
          $data = array(
            "status" =>false,
            "data"=>null,
            "message" => "Invalid Otp",
             );
          return response()->json($data);
        }
      }else
      {
        $data = array(
        "status" =>false,
        "message" => "All Data Required",
         );
        return response()->json($data);
      }

     }
}
