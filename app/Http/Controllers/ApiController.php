<?php

namespace Distributor\Http\Controllers;

use Illuminate\Http\Request;
use LiveGate\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Log;

class ApiController extends Controller
{
    /*
    | login  api
    */
    public function login(Request $request)
    {
      header('Content-Type: application/json');

      //logout params
      if (isset($request['isLogout'])) {
        $isLogout = $request['isLogout'];
      }

      if( isset($request['mobile']) && $request['mobile']!="")
      {       
             // check mobile no exist
             $MobileExist=DB::table('customers')  
             ->where('mobile','=',$request['mobile'])
             ->where('deleted_flag',0)
             ->get();
  
             if (count($MobileExist)>0) 
             {
               
               //static otp
               $otp = 1234;
               // save otp 
               $dataOtp = array('otp' => $otp, 'mobile' =>$request['mobile']);
               // save otp 
               DB::table('saveotp')->insert($dataOtp);
               $lastId = DB::getPdo()->lastInsertId();
               // if set insert id
               if(isset($lastId))
               {
         	         $data = array("success" =>true,"otp"=>$otp,"message" => "Send Otp Successfully");
          			   return response()->json($data);
               }
               else
               {
                  $data = array("success" =>false,"message" => "Data Not Found");
        			    return response()->json($data);
        		   }
             }else
             {
                $data = array("success" =>false,"message" => "Mobile no is not exist");
                return response()->json($data);
             }
          
      }elseif ( $isLogout == 1 ) 
      {
            //user logged out 
            $rows = DB::table('fcm_customer')->where('fcmKey', $fcmKey)->where('mobile', $mobile)->delete();

            if ($rows) {
                        $data = array("success" =>true,"message" => "Log Out Successfully");
                        return response()->json($data);
               }else{
                        $data = array("success" =>false,"message" => "Data Not Found!");
                        return response()->json($data);
               }   
      }else
      {
          $data = array("success" =>false,"message" => "All Data Required");
    			return response()->json($data);
       }	
     }

     /*
       otp verify Otp user if otp is currect send user data
     */
     public function verifyOtp(Request $request){
      header('Content-Type: application/json');

      if( isset($request['mobile']) && $request['mobile']!="" && isset($request['otp']) && $request['otp']!="")
      {
        //check otp on saveotp table
        $check_otp=DB::table('saveotp')
        ->where('mobile','=',$request['mobile'])
        ->where('otp','=',$request['otp'])
        ->get();
        
        if ($check_otp) 
        {

            // verify after delete entry
            DB::table('saveotp')->where('mobile','=',$request['mobile'])
            ->where('otp','=',$request['otp'])->delete();

            //send user  data 
            $UserData=DB::table('customers as c')
            ->leftJoin('users as u', 'c.manager_id', '=', 'u.id')
            ->rightJoin('society_building as b', 'c.building', '=', 'b.building_id')
            ->select('c.*','u.society_name','u.latitude','u.longitude','b.building_name')
            ->where('c.mobile','=',$request['mobile'])
            ->get();



             // send visitor type's
             $visitorTypeData =DB::table('visitor_type') 
             ->where('deleted_flag',0)
             ->get();
             $visitorTypeList = array();
           
             foreach ($visitorTypeData as $key => $visitorType) {
               $visitorTypeList[] = array('id' =>$visitorType->id,
                                          'type' =>$visitorType->type,
                                          'logo' =>$visitorType->logo 
                                        );
             }

            $data = array("success" =>true,"data"=>$UserData,"visitorType"=>$visitorTypeList,"message" => "login Successfully");
            return response()->json($data);

        }else{
             $data = array("success" =>false,"data"=>null,"message" => "Invalid Otp");
             return response()->json($data);
        }
      }else{
         $data = array("success" =>false,"message" => "All Data Required");
         return response()->json($data);
      }

     }

    /* 
     FCM Register and Update of User
    */
    public function fcm_register(Request $request) {

        header('Content-Type: application/json');

        $email = $request['email'];
        $account_id = $request['account_id'];
        $password_token = $request['password_token'];

        $unique_phone_id = $request['unique_phone_id'];
        $fcm_server_id = $request['fcm_server_id'];
        $device_details = $request['device_details'];

        $phone_type = $request['phone_type'];
        $fcmKey = $request['fcmKey'];

        $mobile = $request['mobile'];
        
        if ( empty($mobile) || empty($account_id) || empty($password_token) ||  empty($fcmKey) || empty($phone_type) ) {
            
            return json_encode( array('error' => true, 'message'=>'Invalid Parameters', 'mobile' => $mobile,'id' => $account_id, 'password_token' => $password_token) );
        }
       //Verify user
       $user = $this->verifyUser($account_id, $mobile);

        // if no user, send error    
        if (empty($user) && count($user) < 1 )   {
          //for API call, 
          //return invalid credentails error for user not found in db or invalid password or email all cases
          return json_encode(array('success' => false, 'message'=>'Invalid Credentials', 'mobile' => $mobile ));
        }
        //Always try to update first on the basis of device phone id if its failed then insert new registration id
        if ( !empty($unique_phone_id)  ) {
         
          $message = "Welcome To The LiveGate App";
          // $notificationData = array('title' => 'Welcome To The LiveGate App',
          //                           'message'=>'Welcome To The LiveGate App',
          //                           'image'=>'',
          //                           'action'=>'url',
          //                           'action_destination'=>'login' 
          //                        );
          // push PushNotification call 
          $app_type ='user';
          $notification_status = PushNotification($fcmKey,$message,$mobile,$app_type);

          // set notification status 
          if ($notification_status === 1) {
            $notification_status = true;
          }elseif ($notification_status === 0) {
           $notification_status = false;
          }
          
          $affectedRows = DB::table('fcm_customer')
            ->where('mobile', $mobile)
            ->update(['fcmKey' => $fcmKey, 'device_details' => $device_details, 'updated_at' => date('Y-m-d H:i:s')]);
            
            if ( $affectedRows > 0 ) {

                return json_encode(array('success' => true, 'message' => 'Notification key updated!', 'fcm_server_id' => 0, 'notification_status'=>$notification_status ));

            }else{

                $id = DB::table('fcm_customer')->insertGetId(
                    ['email' => $email, 'mobile'=> $mobile, 'account_id' => $account_id, 'unique_phone_id' => $unique_phone_id, 'fcmKey' =>$fcmKey, 'phone_type' => $phone_type, 'device_details' => $device_details,'pushnotificationios'=>$message, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s') ]
                );

                return json_encode(array('success' => true, 'message' => 'Notification key created.', 'fcm_server_id' => $id,'notification_status'=>$notification_status ));    
            }

        }else{

            return json_encode(array('success' => false, 'message' => 'Empty phone id', 'fcm_server_id' => 0 ));    
        }
   }

    /*
     Verify API user
    */
    function verifyUser($account_id, $mobile)
    {
      //verify user only
        //DB::connection()->enableQueryLog();
        $user = array();
        $user = DB::table('customers')->where('cust_id',$account_id)->where('mobile', $mobile)->first();
        //$queries = DB::getQueryLog();
        return $user->cust_id;
    }
    

    
    



}
