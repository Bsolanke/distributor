<?php

namespace Distributor\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //SUPER ADMIN processing for view 
        if ( Auth::user()->is_super_admin === 1){

            // pass view with society data 
            return view('home');

        }
        if ( Auth::user()->is_vendor === 1){

            //dd(session('user_admin'));
            // pass view with society data 
            return view('home');

        }
        // else if ( Auth::user()->is_admin === 1){
        //      // current admin
        //      $id =Auth::user()->id;

        //      $UserData = DB::table('customers')
        //     ->where('manager_id',$id)
        //     ->where('is_user',1)
        //     ->where('deleted_flag',0)
        //     ->get();
        //     //security
        //      $SecurityData = DB::table('security')
        //     ->where('manager_id',$id)
        //     ->where('is_security',1)
        //     ->where('deleted_flag',0)
        //     ->get();

        //     // pass view with society data 
        //     return view('Admin/home',['UserData'=>$UserData,"SecurityData"=>$SecurityData]);          

        // }else if ( Auth::user()->is_user === 1){
        //     //$user =Auth::user()->is_user;
        //     //echo "is user amdin -".$user;

        // }

        //return view('home');
    }

    public function adminUserLogin(Request $request)
    {
        $id = $request->input('id');
        $email = $request->input('email');

        //dd("id - ".$id." email - ".$email);

        session_unset('user_admin');
        session(['user_admin' => Auth::id()]);
        
        Log::info("Admin user id ".Auth::id());
        
        //login to admin panel
        Auth::loginUsingId($id);
        
        return redirect('home');   
    }
}
