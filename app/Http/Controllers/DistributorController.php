<?php

namespace Distributor\Http\Controllers;

use Illuminate\Http\Request;
use Distributor\Http\Controllers\Controller;
//use Distributor\UserModel\ApiUserModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Distributor\Imports\CustomersImport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Auth;

class DistributorController extends Controller
{     
    public function __construct()
    {
        $this->middleware('auth');
    }
    /*
       load view customer index
    */
    public function index(){

     	return view('Distributor');
    }
    /*
     get all Vendor send view
    */
    public function GetAllDistributor(){
      // Current Auth id
      $id = Auth::user()->id;
      $datasql=DB::table('users')
      ->where('manager_id',Auth::user()->id)
      ->where('is_distributor',1)
      ->get();
      // set data 
      $data = array();
      foreach ($datasql as $row) {

        $data['data'][] = array('id'=>$row->id,
                                'name' => $row->name,
                                'email'=>$row->email,
                                'mobile' => $row->mobile,
                                'address'=>$row->address
                              );
      }
      if (!empty($datasql)) {
        echo json_encode($data);
      }else{
        
       echo "{\"data\":[]}";
      }
    

    }
    /*
      validator add  Distributor form
    */
    public function ValidatorDistributor($request){
    
      $this->validate($request,[
            'name' => 'required|min:3|max:225',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|max:6',
            'confirm_password' => 'required|max:6|same:password',
            'mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:users,mobile'
          ],[
            'name.required' => 'The name field is required.',
            'name.min' => 'The name must be at least 3 characters.',
            'mobile.required' =>'The name field is required.',
            'mobile.min' =>'Required 10 digits, match requested format!.'
          ]);

    }
    /*
      Create new Distributor 
    */
    public function Create(Request $request){

        //validate function call
        $this->ValidatorDistributor($request);

        $name = $request['name'];
        $email = $request['email'];
        $mobile = $request['mobile'];
        $address = $request['address'];
        $password = $request['password'];

        $data = array('name' => $name,
                      'mobile'=>$mobile,
                      'email'=>$email,
                      'is_distributor'=>'1',
                      'manager_id'=> Auth::user()->id,
                      'address'=>$address,
                      'created_at' => date('Y-m-d H:i:s'),
                      'updated_at' => date('Y-m-d H:i:s'),
                      'password'=> Hash::make($password) 
                    );

          $insertId = DB::table('users')->insertGetId($data);

          if($insertId!=0){
                return redirect('Add-Distributor')->with('success',''.$name.' Added Successfully.');
          }else
          {
               return redirect('Add-Distributor')->with('error','Distributor Not Added.');
          }
                    
  }

  /*
      Update Distributor 
  */
  public function Update(Request $request){

        $name = $request['name'];
        $email = $request['email'];
        $mobile = $request['mobile'];
        $address = $request['address'];
        $id = $request['id'];

        $this->validate($request,[
            'name' => 'required|min:3|max:225',
            'email' => 'required|email|unique:users,id,$id',
            'mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:users,id,$id',
            'address' => 'min:3|max:400'
            
          ]);

        $data = array('name' => $name,
                      'mobile'=>$mobile,
                      'email'=>$email,
                      'address'=>$address,
                      'updated_at' => date('Y-m-d H:i:s')
                    );

          $insertId = DB::table('users')->where('id',$id)->update($data);

          if($insertId!=0){
                return redirect('Distributor/'.$id.'')->with('success',''.$name.' update successfully.');
          }else
          {
               return redirect('Distributor/'.$id.'')->with('error','Vendor Not update.');
          }
                    
  }
  /**
   Edit Distributor viwe return 
  **/
  public function editDistributor($id){

    $distributor = DB::table('users')
    ->where('id',$id)
    ->first();

    return view('editvistributor',['distributor'=>$distributor]);
  }
  /*
     delete Distributor
  */ 
  public function DeleteDistributor(Request $request){

    $name = DB::table('users')->where('id',$request['id'])->value('name');

    $sqlDelete = DB::table('users')
    ->where('id',$request['id'])
    ->delete();
    

    if ($sqlDelete) {
      $data = array('success' => true,'message'=>''.$name.' Deleted Successfully.');
      return response()->json($data); 
    }else{
      $data = array('success' => false,'message'=>'Distributor Not Deleted!');
      return response()->json($data);
    }

  } 
  

}
