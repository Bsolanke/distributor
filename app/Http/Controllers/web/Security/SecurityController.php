<?php

namespace Distributor\Http\Controllers\web\Security;

use Illuminate\Http\Request;
use Distributor\Http\Controllers\Controller;
//use Distributor\UserModel\ApiUserModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Auth;

class SecurityController extends Controller
{
   /*
    security view load 
   */
    public function index(){

     	return view('Security/Security');

    }
    /*
     get all Security 
    */ 
    public function GetAllSecurity(){
      $id = Auth::user()->id;
      $datasql=DB::table('security')
      ->where('manager_id',$id)
      ->where('is_security',1)
      ->where('deleted_flag',0)
      ->get();
      // set data 
      $data = array();
      foreach ($datasql as $row) {
        

        $data['data'][] = array('id'=>$row->secu_id,'security_name' => $row->security_name, 'email'=>$row->email,'mobile' => $row->mobile, 'security_gate'=>$row->security_gate,'security_office'=>$row->security_office,'building'=>$row->building);
      }
    echo json_encode($data);

    }
    /*
     Security form valdator 
    */
    public function ValidatorUsers($request){
    
      $this->validate($request,[
            'security_name' => 'required|min:3|max:225',
            'email' => 'sometimes|email|unique:security,email',
            'password' => 'required|max:6',
            'confirm_password' => 'required|max:6|same:password',
            'mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:security,mobile'
          ],[
            'security_name.required' => 'The name field is required.',
            'security_name.min' => 'The name must be at least 3 characters.',
            'mobile.required' =>'The name field is required.',
            'mobile.min' =>'Required 10 digits, match requested format!.'
          ]);

    }
   /*
    Create new Security 
   */ 
   public function CreateSecurity(Request $request){

        //validate function call
        $this->ValidatorUsers($request);

        $security_name = $request['security_name'];
        $email = $request['email'];
        $mobile = $request['mobile'];
        $security_gate = $request['security_gate'];
        $security_office = $request['security_office'];
        $building = $request['building'];
        $password = $request['password'];

        // get auth id 
        $auth_id = Auth::user()->id;

        $data = array('security_name' => $security_name,'mobile'=>$mobile,'email'=>$email,'security_gate'=>$security_gate,'security_office'=>$security_office,'building'=>$building,'is_security'=>'1','manager_id'=>$auth_id,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'password'=> Hash::make($password) );

          $insertId = DB::table('security')->insertGetId($data);

          if($insertId!=0){
                return redirect('AddSecurityView')->with('success','Security Added Successfully.');
          }else
          {
               return redirect('AddSecurityView')->with('error','Security Not Added.');
          }
                    
   }

   /*
    Edit Security view load send data  
   */ 
   public function EditSecurity(Request $request){

    $GetUserData = DB::table('security')
    ->where('secu_id',$request['id'])
    ->where('is_security',1)
    ->get();
    $data = array();
      foreach ($GetUserData as $row) {
        
        $data = array('secu_id'=>$row->secu_id,'security_name' => $row->security_name, 'email'=>$row->email,'mobile' => $row->mobile, 'security_gate'=>$row->security_gate,'security_office'=>$row->security_office,'building'=>$row->building);
      }
  
    return view('Security/EditSecurity')->with ('data',$data);
   }

   /*
    Update Security
   */ 
   public function UpdateSecurity(Request $request){
         $user_id = $request['user_id'];
   
         //validate  
         $this->validate($request,[
            'security_name' => 'required|min:3|max:225',
            //"email' => 'required|email|unique:users,email,$user_id",
            //'mobile' => 'required|min:10|unique:security,mobile',
            'password' => 'sometimes|max:6',
            'confirm_password' => 'sometimes|max:6|same:password'
          ],[
            'security_name.required' => 'The name field is required.',
            'security_name.min' => ' The name must be at least 3 characters.',
            //'mobile.required' =>'The name field is required.',
            //'mobile.min' =>'Required 10 digits, match requested format!.'
          ]);

        $security_name = $request['security_name'];
        $email = $request['email'];
        $mobile = $request['mobile'];
        $security_gate = $request['security_gate'];
        $security_office = $request['security_office'];
        $building = $request['building'];
        $password = $request['password'];
        

        $data = array('security_name' => $security_name,'mobile'=>$mobile,'email'=>$email,'security_gate'=>$security_gate,'security_office'=>$security_office,'building'=>$building,'updated_at' => date('Y-m-d H:i:s'),'password'=> Hash::make($password) );

        
        $sqlUpdate = DB::table('security')->where('secu_id',$user_id)->update($data);
        // update true
        if ($sqlUpdate > 0) {
          return redirect('Security')->with('success','Security Update Successfully.');
        }else
        {
          return redirect('EditSecurity?id='.$user_id.'')->with('error','Security Not Update!.');
        }
   }
   /*
    Delete Sequery 
   */ 
   public function DeleteSecurity(Request $request){

    $updateData = array('deleted_flag' =>'1');
    $sqlDelete = DB::table('security')
    ->where('secu_id',$request['id'])
    ->update($updateData);

    if ($sqlDelete >0) {
      $data = array('success' => true,'message'=>'Security Deleted Successfully.');
      return response()->json($data); 
    }else{
      $data = array('success' => false,'message'=>'Security Not Deleted!');
      return response()->json($data);
    }

   }    

}
