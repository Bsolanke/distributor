<?php

namespace Distributor\Http\Controllers\web\SuperAdmin;

use Illuminate\Http\Request;
use Distributor\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Distributor\Imports\SocietyImport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Auth;

class VisitorController extends Controller
{
    /*
     get all visitor type
    */
    public function GetAllVisitorType(Request $request){

      $datasql=DB::table('visitor_type')
      ->where('deleted_flag',0)
      ->get();

      $data = array(); // array data 
      foreach ($datasql as $row) {
        
        $data['data'][] = array('id'=>$row->id,'type'=>$row->type,'visitor_logo'=>$row->logo,'created_at'=>$row->created_at,'updated_at'=>$row->created_at);
      }
      return response()->json($data);
    }
    /*
     get all company data 
    */
    public function AllCompany(Request $request){

      $datasql=DB::table('visitor_company')
      ->where('deleted_flag',0)
      ->get();

      $data = array(); // array data 
      foreach ($datasql as $row) {
          
        $visitor_type_name = DB::table('visitor_type')->where('id',$row->id)->first(); // visitor type 

        $data['data'][] = array('id'=>$row->id,'company_name'=>$row->company_name,'type'=>$visitor_type_name->type,'type_id'=>$row->type,'company_logo'=>$row->company_logo,'created_at'=>$row->created_at,'updated_at'=>$row->created_at);
      }
      return response()->json($data);
    }

    /*
       Add new Company
    */
    function AddComapny(Request $request)
    {
    
       $company_name = $request['company_name'];
       $visitor_type = $request['visitor_type'];

       $validation = Validator::make($request->all(), [
        'company_logo' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        'company_name' => 'required'
       ]);

       if($validation->passes())
       {
          $image = $request->file('company_logo');

          $new_name = rand() . '.' . $image->getClientOriginalExtension();
          $image->move(public_path('upload/companyLogo/'), $new_name);
          $path = 'upload/companyLogo/'.$new_name;

          $data = array('type'=>$visitor_type,'company_name' => $company_name,'company_logo'=>$path,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s') );

          $sqlInsertId = DB::table('visitor_company')->insertGetId($data);

          $Mesaage = array('status'=>'success','message' => 'Company Add Successfully','class_name'  => 'alert-success');
          return response()->json($Mesaage);
       }
       else
       {
          $Mesaage = array('status'=>'false','message' => $validation->errors()->all(),'class_name'  => 'alert-danger');
          return response()->json($Mesaage);
       }

    }


    /*
      Add New Visitor Type
    */
    function AddVisitorType(Request $request)
    {
       $type = $request['type'];

       $validation = Validator::make($request->all(), [
        'logo' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        'type' => 'required'
       ]);

       if($validation->passes())
       {
          $image = $request->file('logo');

          $new_name = rand() . '.' . $image->getClientOriginalExtension();
          $image->move(public_path('upload/companyLogo/'), $new_name);
          $path = 'upload/companyLogo/'.$new_name;

          $data = array('type' => $type,'logo'=>$path,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s') );

          $sqlInsertId = DB::table('visitor_type')->insertGetId($data);

          $Mesaage = array('status'=>'success','message' => 'Visitor Type Add Successfully','class_name'  => 'alert-success');
          return response()->json($Mesaage);
       }
       else
       {
          $Mesaage = array('status'=>'false','message' => $validation->errors()->all(),'class_name'  => 'alert-danger');
          return response()->json($Mesaage);
       }

    }
    /*
      Delete Visitor Type
    */
    function DeleteVisitorType(Request $request)
    {
       $id = $request['id'];
       if (isset($id) && $id !="") {

          $sql = DB::table('visitor_type')
          ->where('id',$id)
          ->update(['deleted_flag'=>'1']);

           if (count($sql) >0) {
                  $data = array('success' => true,'message'=>'Visitor Type Deleted Successfully.');
                  return response()->json($data); 
           }else{
                  $data = array('success' => false,'message'=>'Visitor Type Not Deleted!');
                  return response()->json($data);
            }
       }else{
            $data = array('success' => false,'message'=>'Invalid Parameters!');
            return response()->json($data);
       }

    }
    /*
      Delete Company
    */
    function DeleteCompany(Request $request)
    {
       $id = $request['id'];
       if (isset($id) && $id !="") {

          $sql = DB::table('visitor_company')
          ->where('id',$id)
          ->update(['deleted_flag'=>'1']);

           if (count($sql) >0) {
                  $data = array('success' => true,'message'=>'Company Deleted Successfully.');
                  return response()->json($data); 
           }else{
                  $data = array('success' => false,'message'=>'Company Not Deleted!');
                  return response()->json($data);
            }
       }
    }


}
