<?php

namespace Distributor\Http\Controllers\web\SuperAdmin;

use Illuminate\Http\Request;
use Distributor\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Distributor\Imports\SocietyImport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Auth;

class SocietyController extends Controller
{
    /*
     load view society
    */
    public function index(){

   	 return view('SuperAdmin/Society');

     }
    /*
     get all society data view
    */
    public function GetAllSociety(Request $request){

      $datasql=DB::table('users')
      ->where('is_admin',1)
      ->where('deleted_flag',0)
      ->get();
      $url = $request->url();
      // set data 
      $data = array();
      foreach ($datasql as $row) {
        
        $data['data'][] = array('id'=>$row->id,'society_name' => $row->society_name, 'email'=>$row->email, 'address'=>$row->address,'society_admin'=>$row->society_admin,'admin_email'=>$row->admin_email,'profile_img'=>$row->profile_img,'path'=>$url,'gate'=>$row->no_of_gate);
      }
    echo json_encode($data);

    }
    /*
     validator from
    */
    public function ValidatorUsers($request){
    
      $this->validate($request,[
            'name' => 'required|min:3|max:225',
            'email' => 'required|email|unique:users',
            'password' => 'required|max:6',
            'confirm_password' => 'required|max:6|same:password',
            'address' => 'required|max:400'
          ],[
            'name.required' => 'The name field is required.',
            'name.min' => ' The name must be at least 3 characters.'
            
          ]);

    }
    /*
     create new society
    */
   public function CreateSociety(Request $request){

        //validate function call
        $this->ValidatorUsers($request);

        $name = $request['name'];
        $email = $request['email'];
        $societyadmin = $request['societyadmin'];
        $adminemail = $request['adminemail'];
        $password = $request['password'];
        $confirmedpass = $request['confirmedpass'];
        $numberofgate = $request['numberofgate'];
        $address = $request['address'];

        // get auth id 
        $auth_id = Auth::user()->id;

        $data = array('society_name' => $name,'name'=>$name,'email'=>$email,'admin_email'=>$adminemail,'society_admin'=>$societyadmin,'no_of_gate'=>$numberofgate,'address'=>$address,'is_admin'=>'1','manager_id'=>$auth_id,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'password'=> Hash::make($password) );

          $insertId = DB::table('users')->insertGetId($data);

          if($insertId!=0){
                return redirect('AddSocietyView')->with('success','Society Added Successfully.');
          }else
          {
               return redirect('AddSocietyView')->with('error','Society Not Added.');
          }
                    
   }
  /*
   edit society view to send data 
  */
   public function EditSociety(Request $request){

    $GetUserData = DB::table('users')
    ->where('id',$request['id'])
    ->where('is_admin',1)
    ->get();
    $data = array();
      foreach ($GetUserData as $row) {
        
        $data = array('id'=>$row->id,'society_name' => $row->society_name, 'email'=>$row->email,'gate' => $row->no_of_gate, 'address'=>$row->address,'society_admin'=>$row->society_admin,'admin_email'=>$row->admin_email,'profile_img'=>$row->profile_img);
      }
    return view('SuperAdmin/EditSociety')->with ('data',$data);
   }
  /*
    Update society
  */
   public function UpdateSociety(Request $request){
        $user_id = $request['user_id'];
         //validate  
         $this->validate($request,[
            'name' => 'required|min:3|max:225',
            "email' => 'required|email|unique:users,email,$user_id",
            'password' => 'sometimes|max:6',
            'confirm_password' => 'sometimes|max:6|same:password',
            'address' => 'required|max:400'
          ],[
            'name.required' => 'The name field is required.',
            'name.min' => ' The name must be at least 3 characters.'
          ]);

        //$this->ValidatorUsers($request);

        $name = $request['name'];
        $email = $request['email'];
        $societyadmin = $request['societyadmin'];
        $adminemail = $request['adminemail'];
        $password = $request['password'];
        $confirmedpass = $request['confirmedpass'];
        $numberofgate = $request['numberofgate'];
        $address = $request['address'];
        

        $data = array('society_name' => $name,'name'=>$name,'email'=>$email,'admin_email'=>$adminemail,'society_admin'=>$societyadmin,'no_of_gate'=>$numberofgate,'address'=>$address,'updated_at' => date('Y-m-d H:i:s'),'password'=> Hash::make($password) );

        $sqlUpdate = DB::table('users')->where('id',$user_id)->update($data);
        // update true
        if ($sqlUpdate > 0) {
          return redirect('Society')->with('success','Society Update Successfully.');
        }else
        {
          return redirect('EditSociety?id='.$user_id.'')->with('error','Society Not Update!.');
        }
   }
  /*
   Delete society 
  */
   public function DeleteSociety(Request $request){

    $updateData = array('deleted_flag' =>'1');
    $sqlDelete = DB::table('users')
    ->where('id',$request['id'])
    ->update($updateData);

    if ($sqlDelete >0) {
      $data = array('success' => true,'message'=>'Society Deleted Successfully.');
      return response()->json($data); 
    }else{
      $data = array('success' => false,'message'=>'Society Not Deleted!');
      return response()->json($data);
    }

   }

   /*
    Upload Excel Society
   */
   public function import() 
    {
        Excel::import(new SocietyImport,request()->file('file'));
           
        return back();
    }

}
