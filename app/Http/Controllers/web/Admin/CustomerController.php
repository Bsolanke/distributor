<?php

namespace Distributor\Http\Controllers\web\Admin;

use Illuminate\Http\Request;
use Distributor\Http\Controllers\Controller;
//use Distributor\UserModel\ApiUserModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Distributor\Imports\CustomersImport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Auth;

class CustomerController extends Controller
{
     /*
       load view customer index
     */
    public function index(){

     	return view('Admin/Customer');

    }
    /*
     get all customer send view
    */
    public function GetAllCustomer(){
      // Current Auth id
      $id = Auth::user()->id;
      $datasql=DB::table('customers')
      ->where('manager_id',$id)
      ->where('is_user',1)
      ->where('deleted_flag',0)
      ->get();
      // set data 
      $data = array();
      foreach ($datasql as $row) {
        

        $data['data'][] = array('id'=>$row->cust_id,'customer_name' => $row->customer_name, 'email'=>$row->email,'mobile' => $row->mobile, 'owner'=>$row->owner,'flat'=>$row->flat_type,'flat_no'=>$row->flat_no,'building'=>$row->building,'members'=>$row->members);
      }
    echo json_encode($data);

    }
    /*
      validator add customer form
    */
    public function ValidatorUsers($request){
    
      $this->validate($request,[
            'customer_name' => 'required|min:3|max:225',
            'email' => 'required|email|unique:customers,email',
            'password' => 'required|max:6',
            'confirm_password' => 'required|max:6|same:password',
            'mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:customers,mobile'
          ],[
            'customer_name.required' => 'The name field is required.',
            'customer_name.min' => 'The name must be at least 3 characters.',
            'mobile.required' =>'The name field is required.',
            'mobile.min' =>'Required 10 digits, match requested format!.'
          ]);

    }
    /*
      Create new customer 
    */
    public function CreateCustomer(Request $request){

        //validate function call
        $this->ValidatorUsers($request);

        $customer_name = $request['customer_name'];
        $email = $request['email'];
        $mobile = $request['mobile'];
        $members = $request['members'];
        $flat = $request['flat'];
        $owner = $request['owners'];
        $flat_no = $request['flat_no'];
        $building = $request['building'];
        $password = $request['password'];

        // get auth id 
        $auth_id = Auth::user()->id;

        $data = array('customer_name' => $customer_name,'mobile'=>$mobile,'email'=>$email,'members'=>$members,'flat_type'=>$flat,'flat_no'=>$flat_no,'owner'=>$owner,'building'=>$building,'is_user'=>'1','manager_id'=>$auth_id,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'password'=> Hash::make($password) );

          $insertId = DB::table('customers')->insertGetId($data);

          if($insertId!=0){
                return redirect('AddCustomerView')->with('success','Customer Added Successfully.');
          }else
          {
               return redirect('AddCustomerView')->with('error','Customer Not Added.');
          }
                    
      }

    /*
     edit customer send data view
    */
    public function EditCustomer(Request $request){

      $GetUserData = DB::table('customers')
      ->where('cust_id',$request['id'])
      ->where('is_user',1)
      ->get();
      $data = array();
        foreach ($GetUserData as $row) {
          
          $data = array('id'=>$row->cust_id,'customer_name' => $row->customer_name, 'email'=>$row->email,'mobile' => $row->mobile, 'owner'=>$row->owner,'flat'=>$row->flat_type,'flat_no'=>$row->flat_no,'building'=>$row->building,'members'=>$row->members);
        }
      return view('Admin/EditCustomer')->with ('data',$data);
   }

   /*
    update customer
   */
   public function UpdateCustomer(Request $request){
         $user_id = $request['user_id'];

         //validate  
         $this->validate($request,[
            'customer_name' => 'required|min:3|max:225',
            "email' => 'required|email|unique:users,email,$user_id",
            //'mobile' => 'required|min:10|unique:users,mobile,$user_id',
            'password' => 'sometimes|max:6',
            'confirm_password' => 'sometimes|max:6|same:password'
          ],[
            'customer_name.required' => 'The name field is required.',
            'customer_name.min' => ' The name must be at least 3 characters.'
            //'mobile.required' =>'The name field is required.',
            //'mobile.min' =>'Required 10 digits, match requested format!.'
          ]);

        $customer_name = $request['customer_name'];
        $email = $request['email'];
        $mobile = $request['mobile'];
        $members = $request['members'];
        $flat = $request['flat'];
        $owner = $request['owners'];
        $flat_no = $request['flat_no'];
        $building = $request['building'];
        $password = $request['password'];
        

        $data = array('customer_name' => $customer_name,'mobile'=>$mobile,'email'=>$email,'members'=>$members,'flat_type'=>$flat,'flat_no'=>$flat_no,'owner'=>$owner,'building'=>$building,'updated_at' => date('Y-m-d H:i:s'),'password'=> Hash::make($password) );

        $sqlUpdate = DB::table('customers')->where('cust_id',$user_id)->update($data);
        // update true
        if ($sqlUpdate > 0) {
          return redirect('Customer')->with('success','Society Update Successfully.');
        }else
        {
          return redirect('EditCustomer?id='.$user_id.'')->with('error','Society Not Update!.');
        }
   }
   /*
     delete customer
   */ 
   public function DeleteCustomer(Request $request){

    $updateData = array('deleted_flag' =>'1');
    $sqlDelete = DB::table('customers')
    ->where('cust_id',$request['id'])
    ->update($updateData);

    if ($sqlDelete >0) {
      $data = array('success' => true,'message'=>'Customer Deleted Successfully.');
      return response()->json($data); 
    }else{
      $data = array('success' => false,'message'=>'Customer Not Deleted!');
      return response()->json($data);
    }

   } 
   /*
    Import Customers Excel
   */
   public function CustomerImport(){

    Excel::import(new CustomersImport,request()->file('file'));
    return back();

   }   

}
