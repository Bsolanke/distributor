<?php

namespace Distributor\Http\Controllers\web\Admin;

use Illuminate\Http\Request;
use Distributor\Http\Controllers\Controller;
//use Distributor\UserModel\ApiUserModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Distributor\Imports\CustomersImport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Auth;

class NoticeboardController extends Controller
{
     /*
       load view noticeboard index
     */
    public function index(){

     	return view('Admin/Noticeboard');

    }
    /*
     get all notice send view
    */
    public function GetAllNoticeboard(){
      // Current Auth id
      $id = Auth::user()->id;
      $datasql=DB::table('noticeboard')
      ->where('manager_id',$id)
      ->where('deleted_flag',0)
      ->get();
      // set data 
      $data = array();
      foreach ($datasql as $row) {
      
        $data['data'][] = array('id'=>$row->noti_id,'title' => $row->title, 'notice'=>$row->notice,'expriy_date' => $row->expriy_date);
      }
    echo json_encode($data);
    }
    /*
      Create new notice 
    */
    public function CreateNotice(Request $request){

        //validate function call
        $this->ValidatorNotice($request);

        $title = $request['title'];
        $notice = $request['notice'];
        $origDate = $request['expriy_date'];
        $time = $request['time'];
        //chnge formate
        $date = str_replace('/', '-', $origDate );
        $newDate = date("Y-m-d", strtotime($date));
        // convert date & marge date and time
        $ConvertDate = date("Y-m-d H:i:s", strtotime($newDate.' '.$time) );
  
        // get auth id 
        $auth_id = Auth::user()->id;

        $data = array('title' => $title,'notice'=>$notice,'expriy_date'=>$ConvertDate,'manager_id'=>$auth_id,'created_at'=> date('Y-m-d H:i:s'), 'updated_at'=> date('Y-m-d H:i:s'));

        $insertId = DB::table('noticeboard')->insertGetId($data);

        if($insertId!=0){
              return redirect('AddNoticeView')->with('success','Notice Added Successfully.');
        }else
        {
             return redirect('AddNoticeView')->with('error','Notice Not Added.');
        }
                    
      }
    /*
      valdation 
    */
    public function ValidatorNotice($request){

        $this->validate($request,[
            'title' => 'required|min:3|max:225',
            'notice' => 'required|min:3|max:1000',
            'expriy_date' => 'required'
          ],[
            'notice.min' => 'The name must be at least 3 characters.',
            'notice.required' =>'The Notice field is required.',
            'expriy_date.required' =>'The Title field is required.'
          ]);
    }
    /*
     edit notice send data view
    */
    public function EditNotice(Request $request){

      $GetNoticeData = DB::table('noticeboard')
      ->where('noti_id',$request['id'])
      ->where('deleted_flag',0)
      ->get();
      $data = array();
      foreach ($GetNoticeData as $row) {
      // date and time divided
      $time =  date('h:i:s a', strtotime($row->expriy_date));
      $date =  date('d-m-Y', strtotime($row->expriy_date));

      $data = array('id'=>$row->noti_id,'title' => $row->title, 'notice'=>$row->notice,'date' => $date,'time'=>$time);

      }
      return view('Admin/EditNotice')->with('data',$data);
   }

   /*
    update Notice
   */
   public function UpdateNotice(Request $request){
         
        //validate function call
        $this->ValidatorNotice($request);
        // auth id
        $auth_id = Auth::user()->id; 

        $id = $request['noti_id'];
        $title = $request['title'];
        $notice = $request['notice'];
        $origDate = $request['expriy_date'];
        $time = $request['time'];
        //chnge formate
        $date = str_replace('/', '-', $origDate );
        $newDate = date("Y-m-d", strtotime($date));
        // convert date & marge date and time
        $ConvertDate = date("Y-m-d H:i:s", strtotime($newDate.' '.$time) );

        $data = array('title' => $title,'notice'=>$notice,'expriy_date'=>$ConvertDate,'manager_id'=>$auth_id,'updated_at' => date('Y-m-d H:i:s') );
       
        $sqlUpdate = DB::table('noticeboard')->where('noti_id',$id)->update($data);
  
        if ($sqlUpdate > 0) {
          return redirect('Noticeboard')->with('success','Notice Update Successfully.');
        }else
        {
          return redirect('Noticeboard')->with('error','Notice Not Update!.');
        }
   }
   /*
     delete Notice
   */ 
   public function DeleteNotice(Request $request){

    $updateData = array('deleted_flag' =>'1');
    $sqlDelete = DB::table('noticeboard')
    ->where('noti_id',$request['id'])
    ->update($updateData);

    if ($sqlDelete >0) {
      $data = array('success' => true,'message'=>'Notice Deleted Successfully.');
      return response()->json($data); 
    }else{
      $data = array('success' => false,'message'=>'Notice Not Deleted!');
      return response()->json($data);
    }

   }  

}
