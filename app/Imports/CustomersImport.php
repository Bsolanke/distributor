<?php
   
namespace Distributor\Imports;
   
use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Auth;
    
class CustomersImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
      
        $manager_id = Auth::user()->id;
        $timestamp = date('Y-m-d H:i:s');

        $arr = array(
            'customer_name' => $row['customer_name'],
            'mobile'    => $row['mobile'],
            'email'    => $row['email'], 
            'members'    => $row['members'],
            'flat_type'    => $row['flat'],
            'flat_no' => $row['flat_no'],
            'owner' => $row['owner'],
            'building'    => $row['building'],
            'manager_id'    => $manager_id,
            'is_user' => '1',
            'password'    => Hash::make($row['password']),
            'created_at'=>$timestamp,
            'updated_at'=>$timestamp
            
        );
        // customers insert query
        $sql = DB::table('customers')->insert($arr);
    }
}