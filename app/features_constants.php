<?php

/* Users  */
define('NOTIFICATION_ON', 1010); //user notification On
define('NOTIFICATION_OFF', 1011); //user notification Off

define('IS_SYS_ADMIN', 1011); //is sys admin




//check if sysadmin
function isSysAdmin() 
{
	if ( Auth::user()->is_super_admin == 1 ) {
		return true;
	}

	return false;
}

function isVendor()
{
        if ( Auth::user()->is_vendor == 1 ) {
                return true;
        }

        return false;
}

function isUser() 
{
  if ( Auth::user()->is_user == 1 && ( !isSysAdmin() && !isVendor() ) ) {
    return true;
  }

  return false;
}

function userAccountType()
{
  if ( isSysAdmin() )
    return 'System Admin';
  else if ( isVendor() )
    return 'Administrator';
  else if ( isUser() )
    return 'User';
  else
    return 'Unknow User';
}


function whichUser($user)
{
  if ( $user->isSuperAdmin )
    return 'System Admin';
  else if ( $user->isAdmin )
    return 'Administrator';
  else if ( $user->accountType == 0 )
    return 'User';
  else
    return 'Unknow User';
}

//get UTC seconds
function getUTCSeconds()
{
  //return UTC offset seconds based on user timeZone selection
  $timezone = Auth::user()->time_zone;  
  
  return get_timezone_offset('UTC', $timezone);

  //return 19800;
}